import static org.junit.Assert.*;

import org.junit.Test;

import com.xiongyingqi.utils.ThreadPool;

/**
 * spark_src
 */

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-10 下午4:21:17
 */
public class ThreadPoolTest {

	@Test
	public void test() {
		for (int i = 0; i < 10000; i++) {
			ThreadPool.invoke(new ThreadOne());
		}
		ThreadPool.shutDown();
	}

	static class ThreadOne implements Runnable {
		static int i;
		/**
		 * <br>2013-9-10 下午4:21:38
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run() {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(ThreadOne.i++);
		}
		
	}
}
