/**
 * RichTextTest
 */


import java.awt.SystemColor;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;

import org.jivesoftware.spark.ui.ChatInputEditor;

import com.alee.laf.rootpane.WebFrame;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-8-29 下午3:20:23
 */
public class MainFrame extends WebFrame {
	
	private JScrollPane scrollPane;
	
	public MainFrame() {
		init();
	}
	
	/**
	 * <br>2013-8-29 下午3:29:50
	 */
	private void init() {
		this.setSize(800, 600);
//		EditorPanel textPanel = new EditorPanel();
//		TextPane textPanel = new TextPane();
		ChatInputEditor editor = new ChatInputEditor();
//		JTextPane textPane = new RichTextPane();
		scrollPane = new JScrollPane(editor);
//		scrollPane.setLayout(new BorderLayout());
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBackground(SystemColor.activeCaption);
		this.add(scrollPane);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		new MainFrame();
	}
}
