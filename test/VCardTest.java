import org.apache.commons.codec.binary.Base64;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.packet.VCard;
import org.jivesoftware.spark.SessionManager;
import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.sparkimpl.profile.VCardManager;
import org.junit.Test;

/**
 * spark_src
 */

/**
 * @author XiongYingqi
 * @version 2013-7-4 下午2:51:24
 */
public class VCardTest {
	public XMPPConnection login(){
		XMPPConnection.DEBUG_ENABLED = true;
		
		XMPPConnection connection = new XMPPConnection("10.188.199.3");
		try {
			connection.connect();
			connection.login("熊瑛琪", "111", "测试端");
		} catch (XMPPException e) {
			e.printStackTrace();
		}
		SessionManager sessionManager = SparkManager.getSessionManager();
		sessionManager.initializeSession(connection, "熊瑛琪", "111");
		
		VCardManager vcardManager = SparkManager.getVCardManager();
		VCard vCard = vcardManager.getVCard();
		System.out.println(vCard);
		
//		String a = new String(vCard.getAvatar());
//		System.out.println(a);
		return connection;
	}
	
	@Test
	public void testVCardXml(){
		XMPPConnection connection = login();
	}
	public static void main(String[] args) {
		VCardTest cardTest = new VCardTest();
		cardTest.login();
	}
}
