/**
 * spark_src
 */
package test.com.kingray.spark.plugin.service;

import org.junit.Test;

import com.kingray.spark.plugin.service.Service;
import com.kingray.spark.plugin.service.ServiceException;
import com.kingray.spark.plugin.service.ServiceThread;

/**
 * @author XiongYingqi
 * @version 2013-6-24 下午5:45:35
 */
public class TestServiceThread {
	@Test
	public void testStartServices(){
		Service service = new Service() {
			@Override
			public void startService() throws ServiceException {
				System.out.println("test 1");
			}
		};
		Service service2 = new Service() {
			@Override
			public void startService() throws ServiceException {
				System.out.println("test 2");
			}
		};
		ServiceThread.addService(service, 20);
		ServiceThread.addService(service2, 0);
		ServiceThread.startServices();
	}
}
