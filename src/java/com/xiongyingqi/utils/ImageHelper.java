/**
 * spark_src
 */
package com.xiongyingqi.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
/**
 * 缩略图类（通用） 本java类能将jpg、bmp、png、gif图片文件，进行等比或非等比的大小转换。 具体使用方法
 * compressPic(大图片路径,生成小图片路径,大图片文件名,生成小图片文名,生成小图片宽度,生成小图片高度,是否等比缩放
 * 
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-17 下午3:08:44
 */
public class ImageHelper {

	/**
	 * 缩放图片 <br>
	 * 2013-9-17 下午3:09:00
	 * 
	 * @param img
	 * @param outputWidth
	 * @param outputHeight
	 * @param isProportion
	 * @return
	 */
	public static Image scaleImage(Image img, int outputWidth, int outputHeight,
			boolean isProportion) {
		int newWidth;
		int newHeight;
		if (isProportion) {// 为等比缩放计算输出的图片宽度及高度
			double rate1 = ((double) img.getWidth(null)) / (double) outputWidth + 0.1;
			double rate2 = ((double) img.getHeight(null)) / (double) outputHeight + 0.1;
			// 根据缩放比率大的进行缩放控制
			double rate = rate1 > rate2 ? rate1 : rate2;
			newWidth = (int) (((double) img.getWidth(null)) / rate);
			newHeight = (int) (((double) img.getHeight(null)) / rate);
		} else {
			newWidth = outputWidth;
			newHeight = outputHeight;
		}
		BufferedImage tag = new BufferedImage((int) newWidth, (int) newHeight,
				BufferedImage.TYPE_INT_RGB );

		Graphics2D g = (Graphics2D) tag.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);  
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);  
        // TODO hints待调整 ...  
		g.setColor(Color.white);  
		g.fillRect(0, 0, newWidth, newHeight);  
		/*
		 * Image.SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢
		 */
		g.drawImage(img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH),
				0, 0, null);

		return tag;
	}
	

}
