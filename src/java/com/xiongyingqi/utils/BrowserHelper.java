/**
 * spark_src
 */
package com.xiongyingqi.utils;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.jivesoftware.Spark;
import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.spark.util.WinRegistry;
import org.jivesoftware.spark.util.log.Log;

import com.jtattoo.plaf.JTattooUtilities;
import com.xiongyingqi.resources.Resource;
import com.xiongyingqi.ui.InstallerChromeFrame;
import com.xiongyingqi.utils.FileHelper;

/**
 * 浏览帮助类
 * 
 * @author 瑛琪
 * @version 2013-7-30 上午10:58:53
 */
public class BrowserHelper {

	private static int tryTimes = 0; // 安装次数

	/**
	 * 浏览地址，默认使用chrome，如果chrome不存在，则使用系统默认浏览器 <br>
	 * 2013-7-30 上午11:06:39
	 * 
	 * @param address
	 *            要浏览的地址
	 */
	public static void browserURI(String address) {
		try {
			browserURI(new URI(address));
		} catch (URISyntaxException e) {
			e.printStackTrace();
			Log.error(e);
		}
	}

	/**
	 * 浏览地址，默认使用chrome，如果chrome不存在，则使用系统默认浏览器
	 * 
	 * @param uri
	 *            URI
	 */
	public static void browserURI(final URI uri) {
		if (!Desktop.isDesktopSupported()) {
			return;
		}

		if (JTattooUtilities.isWindows()) { // 如果是windows系统，则检查chrome
			if (InstallerChromeFrame.getInstance(SparkManager.getMainWindow())
					.isVisible()) { // 如果安装等待界面打开了，那么关闭
				InstallerChromeFrame.getInstance(SparkManager.getMainWindow())
						.dispose();
			}

			String chromePath = null;
			try {
				chromePath = WinRegistry
						.readString(
								WinRegistry.HKEY_LOCAL_MACHINE,
								"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\chrome.exe",
								""); // 获取chrome浏览器
				if (chromePath == null || "".equals(chromePath)) { // win8可能有点不同
					chromePath = WinRegistry
							.readString(
									WinRegistry.HKEY_CURRENT_USER,
									"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\chrome.exe",
									""); // 获取chrome浏览器
				}
				// RunInstallerFrameThread.hide();
			} catch (IllegalArgumentException e) {
				Log.error(e);
			} catch (IllegalAccessException e) {
				Log.error(e);
			} catch (InvocationTargetException e) {
				Log.error(e);
			}

			if (chromePath != null && !"".equals(chromePath)) { // 安装了chrome则使用chrome打开
				try {
					Runtime.getRuntime().exec(chromePath + " " + uri); // 使用命令行打开
					// Desktop.getDesktop().open(new File(chromePath + " " +
					// uri));
				} catch (IOException e) {
					try {
						Desktop.getDesktop().browse(uri);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					Log.error(e);
				}
			} else { // 如果没有安装chrome试图安装浏览器
			// RunInstallerFrameThread.show();
				final URL installerFileURL = Resource
						.getProgramResourcePath("chrome.exe");
				if (installerFileURL != null) { // 验证安装包是否存在
					Thread thread = new Thread(new Runnable() {
						@Override
						public void run() {
							// 拷贝chrome并且运行安装
							String chromeInstallerPath = null;// chrome安装包位置
							try {
								chromeInstallerPath = new File(Spark
										.getSparkUserHome(), "chrome.exe")
										.getCanonicalPath();
								FileHelper.copyFile(installerFileURL, new File(
										chromeInstallerPath));
							} catch (IOException e2) {
								e2.printStackTrace();
							}
							if (chromeInstallerPath != null) {
								Process process = null;
								try {
									process = Runtime.getRuntime().exec(
											chromeInstallerPath);
								} catch (IOException e1) {
									e1.printStackTrace();
								}
								try {
									int waitFor = process.waitFor(); // 等待安装完成
								} catch (InterruptedException e) {
									Log.error(e);
								}
								new File(chromeInstallerPath).delete(); // 删除安装文件
								if (tryTimes++ < 1) { // 安装后测试是否成功
									browserURI(uri);
									return;
								}
							}
						}
					});

					thread.start(); // 开始安装

					InstallerChromeFrame.getInstance(
							SparkManager.getMainWindow()).setVisible(true);
				}
				// try {
				// Desktop.getDesktop().browse(uri);
				// } catch (IOException e) {
				// Log.error(e);
				// }
			}
		} else { // 如果是其他系统
			try {
				Desktop.getDesktop().browse(uri);
			} catch (IOException e) {
				Log.error(e);
			}
		}

	}
}
