/**
 * spark_src
 */
package com.xiongyingqi.emotion;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.border.Border;

import com.xiongyingqi.ui.component.ImageLabel;
import com.xiongyingqi.util.EntityHelper;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-7 下午4:04:16
 */
public class EmotionToggleButton extends JPanel implements MouseListener{
	private static final Color COLOR_MOUSE_ENTER = new Color(255, 189, 63, 255);
	private static final Color COLOR_MOUSE_LEAVE = new Color(255, 255, 255, 255);
	private static final Color COLOR_ALPHA = new Color(255, 255, 255, 0);// 透明颜色
	
	private boolean isMouseDown;
	/**
	 * boolean
	 * @return the isMouseDown
	 */
	public boolean isMouseDown() {
		return isMouseDown;
	}
	/**
	 * boolean
	 * @param isMouseDown the isMouseDown to set
	 */
	public void setMouseDown(boolean isMouseDown) {
		this.isMouseDown = isMouseDown;
	}

	private ImageLabel imageLabel;
	
	public EmotionToggleButton(Icon icon){
		super(true);
		imageLabel = new ImageLabel(icon);
		init();
	}
	/**
	 * @param iconFile
	 */
	public EmotionToggleButton(File iconFile) {
		super(true);
		imageLabel = new ImageLabel(iconFile);
		init();
	}
	private void init(){
		this.add(imageLabel);
		setBackground(COLOR_MOUSE_LEAVE);
		this.addMouseListener(this);
	}
	/**
	 * <br>2013-9-7 下午4:06:06
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
	}
	/**
	 * <br>2013-9-7 下午4:06:06
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if(!isMouseDown){
			this.setBackground(COLOR_MOUSE_ENTER);
			isMouseDown = true;
		} else {
			this.setBackground(COLOR_MOUSE_LEAVE);
			isMouseDown = false;
		}
	}
	/**
	 * <br>2013-9-7 下午4:06:06
	 * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
	}
	/**
	 * <br>2013-9-7 下午4:06:06
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		if(!isMouseDown){
			this.setBackground(COLOR_MOUSE_ENTER);
		}
	}
	
	/**
	 * <br>2013-9-7 下午4:06:06
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		if(!isMouseDown){
			this.setBackground(COLOR_MOUSE_LEAVE);
		}
	}
	
	public void doMouseEntered() {
		this.setBackground(COLOR_MOUSE_ENTER);
		isMouseDown = true;
//		this.setBackground(COLOR_MOUSE_LEAVE);
	}
	
	public void doMouseExited() {
		this.setBackground(COLOR_MOUSE_LEAVE);
		isMouseDown = false;
	}
}
