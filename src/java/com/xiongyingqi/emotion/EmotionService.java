/**
 * spark_src
 */
package com.xiongyingqi.emotion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.spark.SessionManager;
import org.jivesoftware.spark.SparkManager;

import com.xiongyingqi.emotion.EmotionIQ.EmotionActionType;
import com.xiongyingqi.utils.EntityHelper;
import com.xiongyingqi.utils.ThreadPool;

/**
 * 表情服务类，用于发送请求
 * 
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-6 下午2:33:03
 */
public class EmotionService implements PacketListener, PacketFilter {
	private XMPPConnection connection;
	/**
	 * XMPPConnection
	 * @return the connection
	 */
	public XMPPConnection getConnection() {
		return connection;
	}

	/**
	 * XMPPConnection
	 * @param connection the connection to set
	 */
	public void setConnection(XMPPConnection connection) {
		this.connection = connection;
	}

	private static final Object LOCK = new Object();
	private static EmotionService singleton;
	private EmotionManager emotionManager = EmotionManager.getInstance();

	private EmotionService() {
//		ThreadPool.startShowState();
		
		SessionManager sessionManager = SparkManager.getSessionManager();
		connection = sessionManager.getConnection();
	}

	public static EmotionService getInstance() {
		synchronized (LOCK) {
			if (singleton == null) {
				singleton = new EmotionService();
			}
		}
		return singleton;
	}

	/**
	 * 向服务器添加表情 <br>
	 * 2013-9-6 下午2:38:50
	 * 
	 * @param emotions
	 * @return
	 */
	public synchronized Packet sendAddEmotion(Collection<Emotion> emotions) {
		EntityHelper.print(emotions);
		EmotionIQ emotionIQ = new EmotionIQ();
		emotionIQ.setEmotions(emotions);
		emotionIQ.setActionType(EmotionActionType.ADD_EMOTION);
		connection.sendPacket(emotionIQ);
		EntityHelper.print(emotionIQ);
		PacketCollector packetCollector = connection.createPacketCollector(new PacketIDFilter(
				emotionIQ.getPacketID())); // 创建结果收集器
		Packet resultPacket = packetCollector.nextResult(); // 获取返回结果
		return resultPacket;
	}

	/**
	 * 向服务器新增表情 <br>
	 * 2013-9-10 下午5:52:52
	 * 
	 * @param emotions
	 * @return
	 */
	public void addEmotion(Collection<Emotion> emotions) {
		Collection<Emotion> emotionsToAdd = new ArrayList<Emotion>(emotions);
		
		i = 0;
		EntityHelper.print(emotionsToAdd.size());
		
		for (Iterator iterator = emotionsToAdd.iterator(); iterator.hasNext();) {
			Emotion emotion = (Emotion) iterator.next();
			ArrayList<Emotion> emotionsSingle = new ArrayList<Emotion>();
			emotionsSingle.add(emotion);
			
//			EmotionResultIQ iq = (EmotionResultIQ) sendAddEmotion(emotionsSingle);
//			Collection<Emotion> emotionResults = iq.getEmotions();
//			for (Iterator iterator2 = emotionResults.iterator(); iterator2.hasNext();) {
//				Emotion emotion2 = (Emotion) iterator2.next();
//				try {
//					emotion2.store();
//					EntityHelper.print(emotion2);
//					emotionManager.emotionAdded(emotion2);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
			
			
			try {
				ThreadPool.invoke(this, getClass().getDeclaredMethod("addEmotionCallBack", Object.class, Throwable.class,
				 		Object[].class), this, getClass().getDeclaredMethod("sendAddEmotion", Collection.class), emotionsSingle);
			} catch (SecurityException e1) {
				e1.printStackTrace();
			} catch (NoSuchMethodException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private int i = 0;
	public void addEmotionCallBack(Object callBackObject, Throwable throwable, Object... parameters){
		if(callBackObject == null && throwable != null){
			throwable.printStackTrace();
			return;
		}
		
		EntityHelper.print(callBackObject);
		
		EmotionResultIQ iq = (EmotionResultIQ) callBackObject;
		Collection<Emotion> emotionResults = iq.getEmotions();
		for (Iterator iterator2 = emotionResults.iterator(); iterator2.hasNext();) {
			Emotion emotion2 = (Emotion) iterator2.next();
			try {
				emotion2.store();
				EntityHelper.print(emotion2);
				EntityHelper.print(++i);
				emotionManager.emotionAdded(emotion2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 向服务器查询表情集合 <br>
	 * 2013-9-6 下午2:38:32
	 * 
	 * @param emotions
	 * @return
	 */
	public Packet sendQueryDetailEmotion(Collection<Emotion> emotions) {
		EmotionIQ emotionIQ = new EmotionIQ();
		emotionIQ.setEmotions(emotions);
		emotionIQ.setActionType(EmotionActionType.QUERY_DETAIL);
		connection.sendPacket(emotionIQ);
		PacketCollector packetCollector = connection.createPacketCollector(new PacketIDFilter(
				emotionIQ.getPacketID())); // 创建结果收集器
		Packet resultPacket = packetCollector.nextResult(); // 获取返回结果
		return resultPacket;
	}

	/**
	 * 向服务器查询表情总数 <br>
	 * 2013-9-6 下午2:38:32
	 * 
	 * @param emotions
	 * @return
	 */
	public Packet sendQueryCountEmotion() {
		EmotionIQ emotionIQ = new EmotionIQ();
		emotionIQ.setActionType(EmotionActionType.QUERY_COUNT);
		connection.sendPacket(emotionIQ);
		PacketCollector packetCollector = connection.createPacketCollector(new PacketIDFilter(
				emotionIQ.getPacketID())); // 创建结果收集器
		Packet resultPacket = packetCollector.nextResult(); // 获取返回结果
		return resultPacket;
	}

	/**
	 * 向服务器查询表情集合 <br>
	 * 2013-9-6 下午2:38:32
	 * 
	 * @param emotions
	 * @return
	 */
	public Packet sendQuerySummaryEmotion(Collection<Emotion> emotions) {
		EmotionIQ emotionIQ = new EmotionIQ();
//		emotionIQ.setEmotions(emotions);
		emotionIQ.setActionType(EmotionActionType.QUERY_SUMMARY);
		connection.sendPacket(emotionIQ);
		PacketCollector packetCollector = connection.createPacketCollector(new PacketIDFilter(
				emotionIQ.getPacketID())); // 创建结果收集器
		Packet resultPacket = packetCollector.nextResult(); // 获取返回结果
		return resultPacket;
	}

	/**
	 * 向服务器同步表情对象，如果本地和服务器的表情对象存在不同的数据，则会双向同步表情数据 <br>
	 * 2013-9-10 下午6:22:24
	 * 
	 * @param emotions
	 * @return 返回服务器比客户端多的数据
	 */
	public Collection<Emotion> synchroEmotions(Collection<Emotion> emotions) {
		Collection<Emotion> emotionsReturn = new ArrayList<Emotion>();// 返回新增的表情对象
		EmotionResultIQ iqCount = (EmotionResultIQ) sendQueryCountEmotion();
		if (emotions.size() != iqCount.getEmotionCount()) {
			EmotionResultIQ iqSummary = (EmotionResultIQ) sendQuerySummaryEmotion(emotions);

			Collection<Emotion> summaryEmotions = iqSummary.getEmotions();// 从服务器查询概述表情对象

			Collection<Emotion> toPullEmotions = new ArrayList<Emotion>();// 要从服务器拉取的表情对象

			if(summaryEmotions != null){
				for (Iterator<Emotion> iterator = summaryEmotions.iterator(); iterator.hasNext();) {
					Emotion emotion = (Emotion) iterator.next();
					if (emotions.contains(emotion)) {// 如果客户端已经存在该表情对象，则从要查询的集合emotions中移除该对象
						emotions.remove(emotion);
					} else { // 如果客户端不存在该表情对象，则增加到要拉取的对象集合toPullEmotions
						toPullEmotions.add(emotion);
					}
				}
			}
			
			if (toPullEmotions.size() > 0) {
				EmotionResultIQ iqDetail = (EmotionResultIQ) sendQueryDetailEmotion(toPullEmotions);
				emotionsReturn = iqDetail.getEmotions();
			}
			if (emotions.size() > 0) {// 剩下服务器不存在的表情对象
				addEmotion(emotions);
			}
		}
		return emotionsReturn;
	}

	/**
	 * <br>
	 * 2013-9-6 下午2:18:31
	 * 
	 * @see org.jivesoftware.smack.filter.PacketFilter#accept(org.jivesoftware.smack.packet.Packet)
	 */
	@Override
	public boolean accept(Packet packet) {
		return true;
	}

	/**
	 * <br>
	 * 2013-9-6 下午2:18:31
	 * 
	 * @see org.jivesoftware.smack.PacketListener#processPacket(org.jivesoftware.smack.packet.Packet)
	 */
	@Override
	public void processPacket(Packet packet) {
		if (packet instanceof EmotionResultIQ) {
			EmotionResultIQ iq = (EmotionResultIQ) packet;
			switch (iq.getActionType()) {
			case ADD_EMOTION:// 如果接收到服务器的广播，则添加表情
				Collection<Emotion> emotions = iq.getEmotions();
				for (Iterator iterator = emotions.iterator(); iterator.hasNext();) {
					Emotion emotion2 = (Emotion) iterator.next();
					emotionManager.emotionAdded(emotion2);
				}
				break;
			default:
				break;
			}
		}
	}
	
}
