/**
 * spark_src
 */
package com.xiongyingqi.emotion;

import java.util.Collection;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.jivesoftware.smack.packet.IQ;

import com.kingray.spark.packet.IQElementMapping;
import com.kingray.spark.packet.KingrayNameSpace;
import com.xiongyingqi.convert.XmlConvert;

/**
 * 与服务器交流数据的实体
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-4 下午5:17:26
 */
public class EmotionIQ extends IQ{
	private static final KingrayNameSpace QUERY_EMOTION = KingrayNameSpace.QUERY_EMOTION;
	
	private EmotionActionType actionType;
	
	private Collection<Emotion> emotions;
	
	private XmlConvert convert;
	
	
	public EmotionIQ(){
		convert = new XmlConvert(IQElementMapping.class);
	}
	/**
	 * EmotionActionType
	 * @return the actionType
	 */
	public EmotionActionType getActionType() {
		return actionType;
	}

	/**
	 * EmotionActionType
	 * @param actionType the actionType to set
	 */
	public void setActionType(EmotionActionType actionType) {
		this.actionType = actionType;
	}

	/**
	 * Collection<Emotion>
	 * @return the emotions
	 */
	public Collection<Emotion> getEmotions() {
		return emotions;
	}

	/**
	 * Collection<Emotion>
	 * @param emotions the emotions to set
	 */
	public void setEmotions(Collection<Emotion> emotions) {
		this.emotions = emotions;
	}

	/**
	 * <br>2013-9-4 下午5:17:41
	 * @see org.jivesoftware.smack.packet.IQ#getChildElementXML()
	 */
	@Override
	public String getChildElementXML() {
		Element root = DocumentHelper.createElement(QUERY_EMOTION.getKey()); // 添加自定义节点
		if(emotions!= null && emotions.size() > 0){
			root = convert.convertVOs(emotions);
		}
		root.addNamespace("", QUERY_EMOTION.getValue()); // 加入命名空间
		if(actionType != null){ // 设置业务类型
			root.addAttribute("actionType", actionType.toString());
		}
		return root.asXML();
	}

	public enum EmotionActionType{
		/**
		 * 添加表情
		 */
		ADD_EMOTION,
		/**
		 * 查询总条数
		 */
		QUERY_COUNT, 
		/**
		 * 查询概要ID
		 */
		QUERY_SUMMARY,
		/**
		 * 查询详细消息
		 */
		QUERY_DETAIL, 
	}
}
