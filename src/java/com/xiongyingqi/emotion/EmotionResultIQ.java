/**
 * spark_src
 */
package com.xiongyingqi.emotion;

import java.util.Collection;
import java.util.HashSet;

import org.dom4j.Element;
import org.jivesoftware.smack.packet.IQ;

import com.kingray.spark.packet.IQElementMapping;
import com.kingray.spark.packet.KingrayNameSpace;
import com.kingray.spark.plugin.vo.MessageVO;
import com.xiongyingqi.convert.XmlConvert;
import com.xiongyingqi.emotion.EmotionIQ.EmotionActionType;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-6 上午11:00:34
 */
public class EmotionResultIQ extends IQ {
	private static final KingrayNameSpace QUERY_EMOTION = KingrayNameSpace.QUERY_EMOTION;
	private Collection<Emotion> emotions;
	private String result;
	private int emotionCount;
	/**
	 * int
	 * @return the emotionCount
	 */
	public int getEmotionCount() {
		return emotionCount;
	}

	/**
	 * int
	 * @param emotionCount the emotionCount to set
	 */
	public void setEmotionCount(int emotionCount) {
		this.emotionCount = emotionCount;
	}

	private EmotionActionType actionType;
	
	/**
	 * xml转换器
	 */
	private XmlConvert convert;
	
	public EmotionResultIQ(){
		convert = new XmlConvert(IQElementMapping.class);
	}
	
	/**
	 * EmotionActionType
	 * @return the actionType
	 */
	public EmotionActionType getActionType() {
		return actionType;
	}

	/**
	 * EmotionActionType
	 * @param actionType the actionType to set
	 */
	public void setActionType(EmotionActionType actionType) {
		this.actionType = actionType;
	}

	/**
	 * Collection<Emotion>
	 * @return the emotions
	 */
	public Collection<Emotion> getEmotions() {
		return emotions;
	}


	/**
	 * Collection<Emotion>
	 * @param emotions the emotions to set
	 */
	public void setEmotions(Collection<Emotion> emotions) {
		this.emotions = emotions;
	}


	/**
	 * String
	 * @return the result
	 */
	public String getResult() {
		return result;
	}


	/**
	 * String
	 * @param result the result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 *  添加表情到结果集
	 * @param messageVO
	 */
	public void addEmotion(Emotion emotion){
		if(this.emotions == null){
			this.emotions = new HashSet<Emotion>();
		}
		this.emotions.add(emotion);
	}

	/**
	 * <br>2013-9-6 上午11:00:34
	 * @see org.jivesoftware.smack.packet.IQ#getChildElementXML()
	 */
	@Override
	public String getChildElementXML() {
		Element element = convert.convertVOs(emotions);
		element.addNamespace("", QUERY_EMOTION.getValue());
		element.addAttribute("result", result);
		element.addAttribute("emotionCount", emotionCount + "");
		return element.asXML();
	}

}
