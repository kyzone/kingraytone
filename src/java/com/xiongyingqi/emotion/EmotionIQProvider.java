/**
 * spark_src
 */
package com.xiongyingqi.emotion;

import java.io.File;
import java.io.IOException;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.kingray.spark.packet.KingrayNameSpace;
import com.kingray.spark.plugin.packet.MessageHistoryResultIQ;
import com.kingray.spark.plugin.packet.MessageHistoryIQ.MessageHistoryActionType;
import com.kingray.spark.plugin.vo.MessageVO;
import com.xiongyingqi.emotion.EmotionIQ.EmotionActionType;
import com.xiongyingqi.utils.EntityHelper;
import com.xiongyingqi.utils.StringHelper;
import com.xiongyingqi.utils.Base64;
import com.xiongyingqi.utils.FileHelper;

/**
 * 将服务器返回的XML内容转换为实体类
 * 
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-5 上午11:11:57
 */
public class EmotionIQProvider implements IQProvider {
	private static final KingrayNameSpace QUERY_EMOTION = KingrayNameSpace.QUERY_EMOTION;

	/**
	 * <br>
	 * 2013-9-5 上午11:12:31
	 * 
	 * @see org.jivesoftware.smack.provider.IQProvider#parseIQ(org.xmlpull.v1.XmlPullParser)
	 */
	@Override
	public IQ parseIQ(XmlPullParser parser) throws Exception {
		EmotionResultIQ packet = new EmotionResultIQ();

		String actionType = parser.getAttributeValue("", "actionType");

		if (StringHelper.notNullAndNotEmpty(actionType)) {
			switch (EmotionActionType.valueOf(actionType)) {
			case ADD_EMOTION:
				packet = parseAddEmotion(parser);
				break;
			case QUERY_COUNT:
				packet = parseQueryCount(parser);
				break;
			case QUERY_SUMMARY:
				packet = parseQuerySummary(parser);
				break;
			case QUERY_DETAIL:
				packet = parseQueryDetail(parser);
				break;
			default:
				break;
			}
		}
		packet.setActionType(EmotionActionType.valueOf(actionType));
		// packet.setSipAcccount(sip);
		// return packet;
		return packet;
	}

	/**
	 * 查询表情概述 <br>
	 * 2013-9-10 下午5:17:53
	 * 
	 * @param parser
	 * @return
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	private EmotionResultIQ parseQuerySummary(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		EmotionResultIQ iq = new EmotionResultIQ();
		boolean done = false;
		while (!done) {
			int eventType = parser.next();
			if (eventType == XmlPullParser.START_TAG) {
				if (parser.getName().equals("emotion")) {
					Emotion emotion = new Emotion();
					String id = parser.getAttributeValue("", "id");
					if (StringHelper.notNullAndNotEmpty(id)) {
						emotion.setId(id);
					}
					iq.addEmotion(emotion);
				}
			} else if (eventType == XmlPullParser.END_TAG) {
				if (parser.getName().equals(QUERY_EMOTION.getKey())) {
					done = true;
				}
			}
		}
		return iq;
	}

	/**
	 * 查询表情总数 <br>
	 * 2013-9-10 下午5:17:50
	 * 
	 * @param parser
	 * @return
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	private EmotionResultIQ parseQueryCount(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		EmotionResultIQ iq = new EmotionResultIQ();

		String emotionCountStr = parser.getAttributeValue("", "emotionCount");// 获取query节点的属性值
		int emotionCount = 0;
		if (emotionCountStr != null) {
			emotionCount = Integer.parseInt(emotionCountStr);
		}
		iq.setEmotionCount(emotionCount);

		return iq;
	}

	/**
	 * 解析查询详情IQ <br>
	 * 2013-9-6 上午10:57:15
	 * 
	 * @param parser
	 * @return
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	private EmotionResultIQ parseQueryDetail(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		EmotionResultIQ iq = new EmotionResultIQ();
		boolean done = false;
		while (!done) {
			int eventType = parser.next();
			if (eventType == XmlPullParser.START_TAG) {
				if (parser.getName().equals("emotion")) {
					Emotion emotion = new Emotion();
					String id = parser.getAttributeValue("", "id");
					String emotionId = parser.getAttributeValue("", "emotionId");
					String emotionString = parser.getAttributeValue("", "emotionString");
					String base64Data = parser.getAttributeValue("", "base64Data");
					if (StringHelper.notNullAndNotEmpty(id)) {
						emotion.setId(id);
					}
					if (StringHelper.notNullAndNotEmpty(emotionId)) {
						emotion.setEmotionId(emotionId);
					}
					if (StringHelper.notNullAndNotEmpty(emotionString)) {
						emotion.setEmotionString(emotionString);
					}
					if (StringHelper.notNullAndNotEmpty(base64Data)) {
						File emotionFile = Emotion.convertToPublicEmotionFile(base64Data, emotionId);
						emotion.setEmotionFile(emotionFile);
					}
					try {
						emotion.store();
						iq.addEmotion(emotion);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else if (eventType == XmlPullParser.END_TAG) {
				if (parser.getName().equals(QUERY_EMOTION.getKey())) {
					done = true;
				}
			}
		}
		return iq;
	}

	/**
	 * 解析添加表情的IQ <br>
	 * 2013-9-6 上午10:57:13
	 * 
	 * @param parser
	 * @return
	 * @throws IOException
	 * @throws XmlPullParserException
	 */
	private EmotionResultIQ parseAddEmotion(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		EmotionResultIQ iq = new EmotionResultIQ();
		String result = parser.getAttributeValue("", "result");// 获取query节点的属性值
		iq.setResult(result);
		boolean done = false;
		while (!done) {
			int eventType = parser.next();
			if (eventType == XmlPullParser.START_TAG) {
				if (parser.getName().equals("emotion")) {
					Emotion emotion = new Emotion();
					String id = parser.getAttributeValue("", "id");
					String emotionId = parser.getAttributeValue("", "emotionId");
					String emotionString = parser.getAttributeValue("", "emotionString");
					String base64Data = parser.getAttributeValue("", "base64Data");
					if (StringHelper.notNullAndNotEmpty(id)) {
						emotion.setId(id);
					}
					if (StringHelper.notNullAndNotEmpty(emotionId)) {
						emotion.setEmotionId(emotionId);
					}
					if (StringHelper.notNullAndNotEmpty(emotionString)) {
						emotion.setEmotionString(emotionString);
					}
					if (StringHelper.notNullAndNotEmpty(base64Data)) {
						File emotionFile = Emotion.convertToPublicEmotionFile(base64Data, emotionId);
						emotion.setEmotionFile(emotionFile);
					}
					try {
						emotion.store();
					} catch (Exception e) {
						e.printStackTrace();
					}
					iq.addEmotion(emotion);
				}
			} else if (eventType == XmlPullParser.END_TAG) {
				if (parser.getName().equals(QUERY_EMOTION.getKey())) {
					done = true;
				}
			}
		}
		return iq;
	}

}
