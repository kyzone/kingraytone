/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.lang.reflect.InvocationTargetException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.xiongyingqi.emotion.Emotion;

/**
 * 放置表情的标签，用于在编辑框内显示的
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-8-30 上午10:45:19
 */
public class EmotionImageLabel extends ImageLabel{
	private Emotion emotion;
	public EmotionImageLabel(Icon icon){
		super(icon);
	}
	
	public EmotionImageLabel(File iconFile){
		super(iconFile);
	}
	public EmotionImageLabel(Emotion emotion){
		super(emotion.getEmotionFile());
		this.emotion = emotion;
	}

	/**
	 * Emotion
	 * @return the emotion
	 */
	public Emotion getEmotion() {
		return emotion;
	}

	/**
	 * Emotion
	 * @param emotion the emotion to set
	 */
	public void setEmotion(Emotion emotion) {
		this.emotion = emotion;
	}
	
	
//	/**
//	 * <br>2013-8-30 上午10:42:56
//	 * @see javax.swing.JComponent#paint(java.awt.Graphics)
//	 */
//	@Override
//	public void paint(Graphics g) {
//		g.drawImage(icon.getImage(), 0, 0, null);
//	}

}
