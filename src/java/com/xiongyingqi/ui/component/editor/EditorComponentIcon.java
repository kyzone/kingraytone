/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

import java.io.File;
import java.util.regex.Pattern;

import com.xiongyingqi.utils.Base64;
import com.xiongyingqi.utils.FileHelper;

/**
 * 图片类型内嵌对象
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-2 下午3:29:24
 */
public class EditorComponentIcon extends AbstractEditorComponent {
	private static final Pattern PATTERN = Pattern.compile("<img[\\s]+?[^<>]+>");
	/**
	 * @param priority
	 */
	public EditorComponentIcon() {
		super(1);
	}

	private File iconFile;
	
	/**
	 * <br>2013-9-2 下午3:29:45
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#getContent()
	 */
	@Override
	public Object getContent() {
		return iconFile;
	}

	/**
	 * <br>2013-9-2 下午3:29:45
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#setContent(java.lang.Object)
	 */
	@Override
	public void setContent(Object content) {
		if(content instanceof File){
			iconFile = (File) content;
		} else {
			throw new ClassCastException("类型错误");
		}
	}
	
	/**
	 * <br>2013-9-2 下午3:39:26
	 * @throws Exception 
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#insertContentToTextPane(javax.swing.JTextPane)
	 */
	@Override
	public void insertContentToTextPane(InputEditor inputEditor) throws Exception {
		if(iconFile != null){
			inputEditor.insertIcon(iconFile);
		} else {
			throw new IconPermissionException("插入的图标为空！");
		}
	}
	
	/**
	 * <br>2013-9-2 下午3:39:44
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#geType()
	 */
	@Override
	public EditorComponentType geType(){
		return EditorComponentType.ICON;
	}

	/**
	 * <br>2013-9-3 下午12:08:25
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#encodeToString()
	 */
	@Override
	public String encodeToString() throws Exception {
		byte[] data = FileHelper.readFileToBytes(iconFile);
		String encodeStr = Base64.encodeBytes(data);// 转换为Base64字符串，方便传输
		StringBuilder builder = new StringBuilder();
		builder.append("<img src='");
		builder.append(encodeStr);
		builder.append("'>");
		
		String rs = builder.toString();
		return rs;
	}

	/**
	 * <br>2013-9-3 下午12:08:25
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#decodeFromString(java.lang.String)
	 */
	@Override
	public EditorComponent decodeFromString(String encoded) throws EditorComponentSupportException {
		return null;
	}

	/**
	 * <br>2013-9-18 上午11:50:49
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#getParsePattern()
	 */
	@Override
	public Pattern getParsePattern() throws EditorComponentSupportException {
		return PATTERN;
	}

}
