/**
 * RichTextTest
 */
package com.xiongyingqi.ui.component.editor;

import java.util.regex.Pattern;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-2 下午3:57:16
 */
public class EditorComponentString extends AbstractEditorComponent {
	
	/**
	 * @param priority
	 */
	public EditorComponentString() {
		super(Integer.MAX_VALUE);
	}

	private String content;
	/**
	 * <br>2013-9-2 下午3:57:27
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#getContent()
	 */
	@Override
	public Object getContent() {
		return content;
	}

	/**
	 * <br>2013-9-2 下午3:57:27
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#setContent(java.lang.Object)
	 */
	@Override
	public void setContent(Object content) {
		if(content instanceof String){
			this.content = (String) content;
		} else {
			throw new ClassCastException("类型错误");
		}
	}

	/**
	 * <br>2013-9-2 下午3:57:27
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#insertContentToTextPane(javax.swing.JTextPane)
	 */
	@Override
	public void insertContentToTextPane(InputEditor inputEditor) throws Exception {
		if(content != null){
			inputEditor.insertString(content);
		}
	}

	/**
	 * <br>2013-9-2 下午3:57:27
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#geType()
	 */
	@Override
	public EditorComponentType geType() {
		return EditorComponentType.STRING;
	}

	/**
	 * <br>2013-9-3 下午12:06:03
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#encodeToString()
	 */
	@Override
	public String encodeToString() throws Exception {
		return content;
	}

	/**
	 * <br>2013-9-3 下午12:06:03
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#decodeFromString(java.lang.String)
	 */
	@Override
	public EditorComponent decodeFromString(String encoded) throws EditorComponentSupportException {
		this.content = encoded;
		return this;
	}

	/**
	 * <br>2013-9-18 下午12:08:55
	 * @see com.xiongyingqi.ui.component.editor.EditorComponent#getParsePattern()
	 */
	@Override
	public Pattern getParsePattern() throws EditorComponentSupportException {
		return null;
	}

}
