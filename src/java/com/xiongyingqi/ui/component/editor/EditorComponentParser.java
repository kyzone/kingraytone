/**
 * spark_src
 */
package com.xiongyingqi.ui.component.editor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
 * @version 2013-9-17 下午6:25:40
 */
public class EditorComponentParser {
	private static EditorComponentParser singleton;
	public static Object LOCK = new Object();
	private Collection<EditorComponent> parsers;
	private static final Comparator<EditorComponent> COMPARATOR = new EditorComponentComparator<EditorComponent>();
	private static final Comparator<MarkTag> MARKTAG_COMPARATOR = new MarkTagComparator();
	
	public static EditorComponentParser getInstance() {
		synchronized (LOCK) {
			if (singleton == null) {
				singleton = new EditorComponentParser();
			}
		}
		return singleton;
	}

	private EditorComponentParser() {
		init();
	}

	private void init() {
		List<EditorComponent> editorComponents = new ArrayList<EditorComponent>();// 初始化所有的编辑器对象
		EditorComponentType[] componentTypes = EditorComponentType.values();
		for (int i = 0; i < componentTypes.length; i++) {
			EditorComponentType editorComponentType = componentTypes[i];
			Class<? extends EditorComponent> clazz = editorComponentType.getEditorComponentClass();
			try {
				EditorComponent editorComponent = clazz.newInstance();
				editorComponents.add(editorComponent);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		Collections.sort(editorComponents, COMPARATOR);// 将编辑器按优先级排序
		parsers = editorComponents;
	}

	/**
	 * 根据已经编码的字符串解析成EditorComponent对象集合 <br>
	 * 2013-9-17 下午6:26:21
	 * 
	 * @param content
	 * @return 解析完成的EditorComponent对象集合
	 */
	public Collection<EditorComponent> parseFromString(String content) {
		List<MarkTag> markTags = new ArrayList<EditorComponentParser.MarkTag>();// 标记已经在字符串内找到的内嵌对象
		Class<? extends EditorComponent> nullPatternComponentClass = null;// 字符串类型的解释器类
		
		for (Iterator<EditorComponent> iterator = parsers.iterator(); iterator.hasNext();) {
			EditorComponent decoder = (EditorComponent) iterator.next();// 担任解释器角色
			Pattern pattern;
			try {
				pattern = decoder.getParsePattern();
				if (pattern != null) {
					Matcher matcher = pattern.matcher(content);
					while(matcher.find()){
						MarkTag markTag = new MarkTag();
						
						String encoded = matcher.group();// 解释器捕获到的内容
						EditorComponent component = decoder.decodeFromString(encoded);// 解码
						
						int startIndex = matcher.start();
						int endIndex = matcher.end();
						
						markTag.setEditorComponent(component);
						markTag.setStartIndex(startIndex);
						markTag.setEndIndex(endIndex);
						
						markTags.add(markTag);
					}
				} else {
					nullPatternComponentClass = decoder.getClass();
				}
			} catch (EditorComponentSupportException e) {
				//由于有已经定义的编辑器内嵌对象没有实现解释器类型，可直接忽略
			}
		}
		// 以MarkTag的startIndex进行排序
		Collections.sort(markTags, MARKTAG_COMPARATOR);
		
		Collection<EditorComponent> results = new ArrayList<EditorComponent>();
		
		//所有解析工作已经完成，只剩下返回pattern为null的类型了，因此可以进行最后的清理工作了
		int startIndex = 0;// 截取字符串的开始位置
		int endIndex = 0;// 截取字符串的结束位置
		for (Iterator<MarkTag> iterator = markTags.iterator(); iterator.hasNext();) {
			MarkTag markTag = iterator.next();
			startIndex = markTag.getStartIndex();
			String stringContent = content.substring(endIndex, startIndex);// 截取没被解释器标记的字符串
			if(nullPatternComponentClass != null){
				try {
					EditorComponent editorComponent = nullPatternComponentClass.newInstance();
					editorComponent.setContent(stringContent);
					results.add(editorComponent);
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			endIndex = markTag.getEndIndex();// 进入下一个标记
			
			results.add(markTag.getEditorComponent());
		}
		
		//可能还会剩下最后的字符串没处理
		int contentLength = content.length();
		if(endIndex != contentLength){
			String stringContent = content.substring(endIndex, contentLength);
			if(nullPatternComponentClass != null){
				try {
					EditorComponent editorComponent = nullPatternComponentClass.newInstance();
					editorComponent.setContent(stringContent);
					results.add(editorComponent);
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}

		return results;
	}

	/**
	 * 标记对象 <br>
	 * 当解释器解析了一段编码的字符串时要先标记其开始位置、结束位置和解释器类型，直到所有的解释器都完成工作时才能将字符串转换成具体的编辑器对象(
	 * EditorComponent)
	 * 
	 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
	 * @version 2013-9-18 下午4:45:23
	 */
	class MarkTag {
		
		/**
		 * 标记起始位置（包括）
		 */
		private int startIndex;
		/**
		 * 标记结束位置（不包括）
		 */
		private int endIndex;
		/**
		 * 对应的解释器
		 */
		private EditorComponent editorComponent;
		/**
		 * int
		 * @return the startIndex
		 */
		public int getStartIndex() {
			return startIndex;
		}
		/**
		 * int
		 * @param startIndex the startIndex to set
		 */
		public void setStartIndex(int startIndex) {
			this.startIndex = startIndex;
		}
		/**
		 * int
		 * @return the endIndex
		 */
		public int getEndIndex() {
			return endIndex;
		}
		/**
		 * int
		 * @param endIndex the endIndex to set
		 */
		public void setEndIndex(int endIndex) {
			this.endIndex = endIndex;
		}
		/**
		 * EditorComponent
		 * @return the editorComponent
		 */
		public EditorComponent getEditorComponent() {
			return editorComponent;
		}
		/**
		 * EditorComponent
		 * @param editorComponent the editorComponent to set
		 */
		public void setEditorComponent(EditorComponent editorComponent) {
			this.editorComponent = editorComponent;
		}
		
		
	}
	static class MarkTagComparator implements Comparator<MarkTag> {
		/**
		 * <br>2013-9-18 下午5:14:35
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(MarkTag o1, MarkTag o2) {
			int value = 0;
			if (o1.getStartIndex() < o2.getStartIndex()) {
				value = -1;
			} else if (o1.getStartIndex() > o2.getStartIndex()) {
				value = 1;
			}
			return value;
		}
	}
	

	/**
	 * 解码优先级排序器，排序号越小越在前
	 * 
	 * @author 瑛琪 <a href="http://xiongyingqi.com">xiongyingqi.com</a>
	 * @version 2013-9-18 上午11:00:07
	 */
	static class EditorComponentComparator<E> implements Comparator<EditorComponent> {
		/**
		 * 按照优先级排序，排序号越小越在前 <br>
		 * 2013-9-18 上午10:55:48
		 * 
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(EditorComponent o1, EditorComponent o2) {
			int value = 0;
			if (o1.getDecodePriority() < o2.getDecodePriority()) {
				value = -1;
			} else if (o1.getDecodePriority() > o2.getDecodePriority()) {
				value = 1;
			}
			return value;
		}

	}

	public static void main(String[] args) {
		EditorComponentParser.getInstance();
	}
}
