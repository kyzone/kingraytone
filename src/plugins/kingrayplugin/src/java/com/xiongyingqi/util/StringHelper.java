/**
 * openfire_src
 */
package com.xiongyingqi.util;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;

import org.jivesoftware.spark.util.WinRegistry;

/**
 * @author KRXiongYingqi
 * @version 2013-6-17 上午11:19:19
 */
public class StringHelper {
	/**
	 * 判断字符串不为空，并且字符串内容不为空
	 * 
	 * @param input
	 * @return
	 */
	public static boolean notNullAndNotEmpty(String input) {
		return input != null && !"".equals(input.trim());
	}

	/**
	 * 判断字符串是否为空，或者字符串内容为空
	 * 
	 * @param input
	 * @return
	 */
	public static boolean nullOrEmpty(String input) {
		return input == null || "".equals(input.trim());
	}

	/**
	 * 换行符 <br>
	 * 2013-8-14 下午1:20:06
	 * 
	 * @return
	 */
	public static String line() {
		String lineSeparator = System.getProperty("line.separator");
		return lineSeparator;
	}
	
	/**
	 * 
	 * <br>2013-8-28 下午5:26:24
	 * @param str
	 * @return
	 */
	public static String convertEncode(String str){
		if(nullOrEmpty(str)){
			return null;
		}
		try {
			return new String(str.getBytes(), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return str;
	}
	
	public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		System.out.println(System.getProperty("line.separator"));
		String chromePath = WinRegistry
				.readString(
						WinRegistry.HKEY_CURRENT_USER,
						"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\chrome.exe",
						"");
		System.out.println(chromePath);
		System.out.println(convertEncode(chromePath));
	}
}
