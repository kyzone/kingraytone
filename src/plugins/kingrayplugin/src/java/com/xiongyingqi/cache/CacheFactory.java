package com.xiongyingqi.cache;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.jivesoftware.spark.util.log.Log;
/**
 *  缓存工厂类，用于管理缓存的生命周期
 * @author XiongYingqi
 */
public class CacheFactory<E> {
	private static Collection<Cache> caches;
	
	private CacheFactory(){
		
	}
	
//	public static Cache getCache(){
//		Cache cache = new Cache();
//		return getCache(cache.getCacheName());
//	}
	
	public static Cache getCache(String cacheId, Class<? extends Cache> cacheClass){
		try {
			Constructor<? extends Cache> constructor = cacheClass.getDeclaredConstructor(String.class);
			Cache cache = constructor.newInstance(cacheId);
//			cacheClass.newInstance();
			return getCache(cache);
		} catch (CacheException e) {
			Log.error(e);
		} catch (InstantiationException e) {
			Log.error(e);
		} catch (IllegalAccessException e) {
			Log.error(e);
		} catch (SecurityException e) {
			Log.error(e);
		} catch (NoSuchMethodException e) {
			Log.error(e);
		} catch (IllegalArgumentException e) {
			Log.error(e);
		} catch (InvocationTargetException e) {
			Log.error(e);
		}
		return null;
	}
	
	public synchronized static Cache getCache(Cache cacheInstance) throws CacheException{
		if(caches == null){
			caches = new HashSet<Cache>();
		}
		if(cacheInstance == null){
			throw new CacheException("Null instance of cache: " + cacheInstance);
		}
		Cache existCache = null;
		for (Iterator iterator = caches.iterator(); iterator.hasNext();) {
			Cache cache = (Cache) iterator.next();
			if(cache.equals(cacheInstance)){
				existCache = cache;
				break;
			}
		}
		if(existCache == null){
			existCache = cacheInstance;
		}
		caches.add(existCache);
		return existCache;
	}
	
	/**
	 * 调用Cache的notifyCacheDispose方法，提醒缓存监听类开始处理缓存的清理
	 */
	public static void notifyCacheListenersDispose(){
		for (Iterator iterator = caches.iterator(); iterator.hasNext();) {
			Cache target = (Cache) iterator.next();
			target.notifyCacheDispose();
		}
	}
	
	public static void main(String[] args) {
			HashSetCache cache = (HashSetCache) CacheFactory.getCache("a", HashSetCache.class);
			cache.add("asfasf");
			cache.add("bsfasf");
			cache.add("csfasf");
			cache.add("dsfasf");
			
			Cache cache2 = CacheFactory.getCache("a", HashSetCache.class);
			Cache cache3 = CacheFactory.getCache("a", HashSetCache.class);
			System.out.println(cache);
			System.out.println(cache2);
			System.out.println(cache3);
			
//			System.out.println(CacheFactory.getCache());
//			cache.addCacheListener(null);
	}
	
}
