package com.xiongyingqi.cache;

import com.xiongyingqi.exception.BaseException;

/**
 * 缓存异常
 * @author XiongYingqi
 * @version 2013-6-24 下午4:58:44
 */
public class CacheException extends BaseException{
	public CacheException(){
		
	}
	public CacheException(String message){
		super(message);
	}
	public CacheException(String message, Throwable throwable){
		super(message, throwable);
	}
	public CacheException(Throwable throwable){
		super(throwable);
	}
}
