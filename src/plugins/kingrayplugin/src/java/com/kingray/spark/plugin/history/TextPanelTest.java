/**
 * spark_src
 */
package com.kingray.spark.plugin.history;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.DefaultEditorKit.CopyAction;
import javax.swing.text.html.HTMLEditorKit;

import org.jivesoftware.spark.SparkManager;

import com.xiongyingqi.util.RegexHelper;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.font.TextHitInfo;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author XiongYingqi
 * @version 2013-7-3 下午2:33:21
 */
public class TextPanelTest extends JFrame{
	JTextPane textPane = new JTextPane();
	
	public TextPanelTest() {
		this.setSize(800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		getContentPane().add(textPane, BorderLayout.CENTER);
		StringBuilder builder = new StringBuilder(textPane.getText());
		
		builder.append("<html>");
		builder.append("<body>");
		builder.append("<span>我那个区</span><br />");
		builder.append("<span>阿康就开始啦减肥<a href='#'>...</a></span><br />");
		builder.append("<span>爱爱爱</span><br />");
		builder.append("</body>");
		builder.append("</html>");
		
		textPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("Ctrl c"), "copy");
		textPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("Ctrl x"), "cut");
		textPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("Ctrl v"), "paste");
		
		textPane.getActionMap().put("copy", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				copyAction(e);
			}
		});
		textPane.getActionMap().put("cut", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				cutAction(e);
			}
		});
		textPane.getActionMap().put("paste", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pasteAction(e);
			}
		});
		
		
//		textPane.addCaretListener(new CaretListener() {
//			@Override
//			public void caretUpdate(CaretEvent e) {
//				int startPoint = e.getDot();
//				int endPoint = e.getMark();
//				if(startPoint == endPoint){
//					return;
//				}
//				if(startPoint > endPoint){
//					startPoint = startPoint + endPoint;
//					endPoint = startPoint - endPoint;
//					startPoint = startPoint - startPoint;
//				}
//				String text = textPane.getText(); // 获取编辑框的文本
//				String beforeText = text.substring(0, startPoint);
//				String afterText = text.substring(endPoint, text.length());
//				
//				String subString = text.substring(startPoint, endPoint); // 截取新加入的字符串
//				System.out.println("subString: " + subString);
//				String processedText = RegexHelper.replaceAllHTMLComment(subString);// 去除注释
//				
//				String rsString = new StringBuilder().append(beforeText).append(processedText).append(afterText).toString();
//				if(!rsString.equals(text)){
//					textPane.setText(rsString);
//				}
//			};
//		});
//		
//		textPane.addHierarchyListener(new HierarchyListener() {
//			
//			@Override
//			public void hierarchyChanged(HierarchyEvent e) {
//			}
//		});
//		
//		textPane.addKeyListener(new KeyAdapter() {
//			/**
//			 * <br>2013-7-3 下午3:58:08
//			 * @see java.awt.event.KeyAdapter#keyReleased(java.awt.event.KeyEvent)
//			 */
//			@Override
//			public void keyReleased(KeyEvent e) {
////				String text = textPane.getText(); // 获取编辑框的文本
////				String processedText = RegexHelper.replaceAllHTMLComment(text);// 去除注释
////				if(!processedText.equals(text)){
////					textPane.setText(processedText);
////				}
//			}
//			/**
//			 * <br>2013-7-3 下午4:02:09
//			 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
//			 */
//			@Override
//			public void keyPressed(KeyEvent e) {
//				
//				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
//				String clipboardName = clipboard.getName();
//				System.out.println(clipboardName);
//				Transferable transferable = clipboard.getContents(null);
//				DataFlavor[] dataFlavors = transferable.getTransferDataFlavors();
//				if(dataFlavors.length > 0){
//					DataFlavor dataFlavor = dataFlavors[dataFlavors.length - 1];
//					System.out.println(dataFlavor);
//				}
////				for (int i = 0; i < dataFlavors.length; i++) {
////					DataFlavor dataFlavor = dataFlavors[i];
////					System.out.println(dataFlavor);
////				}
//			}
//		});
		textPane.setEditable(true);
		textPane.setEditorKit(new HTMLEditorKit());
		textPane.setText(builder.toString());
	}
	/**
	 * 拷贝事件
	 * <br>2013-7-5 上午10:23:12
	 * @param e
	 */
	public void copyAction(ActionEvent e){
		System.out.println("copyAction");
		SparkManager.setClipboard(textPane.getSelectedText());
	}
	/**
	 * 剪切事件
	 * <br>2013-7-5 上午10:23:24
	 * @param e
	 */
	public void cutAction(ActionEvent e){
		System.out.println("cutAction");
		SparkManager.setClipboard(textPane.getSelectedText());
		textPane.replaceSelection("");
	}
	/**
	 * 粘帖事件
	 * <br>2013-7-5 上午10:23:33
	 * @param e
	 */
	public void pasteAction(ActionEvent e){
		System.out.println("pasteAction");
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		DataFlavor[] dataFlavors = clipboard.getAvailableDataFlavors();
		for (int i = 0; i < dataFlavors.length; i++) {
			DataFlavor dataFlavor = dataFlavors[i];
			try {
				Object clipBoardContent = clipboard.getData(dataFlavor);
				
				
//				System.out.println(clipBoardContent.toString());
//				Class clazz = clipBoardContent.getClass();
//				if(clazz.isArray()){
//					
//					Byte[] objects = (Byte[]) clipBoardContent;
//					for (int j = 0; j < objects.length; j++) {
//						Object object = objects[j];
//						System.out.println(object.getClass());
//					}
//					
////					Array array = (Array) clipBoardContent;
////					int arrayLength = Array.getLength(array);
////					for (int i = 0; i < arrayLength; i++) {
////						Object object = Array.get(array, i);
////						System.out.println(object.getClass());
////					}
//				}
				
				if(dataFlavor.isFlavorJavaFileListType()){ // 是否是文件类型
					List list = (List) clipBoardContent;
					for (Iterator iterator = list.iterator(); iterator
							.hasNext();) {
						File file = (File) iterator.next();
						
					}
					System.out.println("isFlavorJavaFileListType");
				} 
				if (dataFlavor.isFlavorRemoteObjectType()) {
					System.out.println("isFlavorRemoteObjectType");
				} 
				
				if (dataFlavor.isFlavorSerializedObjectType()) {
					System.out.println("isFlavorSerializedObjectType");
				} 

				if (dataFlavor.isFlavorTextType()) {
					java.io.InputStreamReader inputStreamReader = (java.io.InputStreamReader) clipBoardContent;
					
					char[] chars = new char[64];
					int length = -1;
					while ((length = inputStreamReader.read(chars)) != -1) {
						System.out.println(length);
					}
//					textPane.replaceSelection();
					System.out.println("isFlavorTextType");
				} 

				if (dataFlavor.isMimeTypeSerializedObject()) {
					System.out.println("isMimeTypeSerializedObject");
				} 

				if (dataFlavor.isRepresentationClassByteBuffer()) {
					System.out.println("isRepresentationClassByteBuffer");
				} 

				if (dataFlavor.isRepresentationClassCharBuffer()) {
					System.out.println("isRepresentationClassCharBuffer");
				} 

				if (dataFlavor.isRepresentationClassInputStream()) {
					System.out.println("isFlavorJavaFileListType");
				} 

				if (dataFlavor.isRepresentationClassReader()) {
					System.out.println("isRepresentationClassReader");
				}

				if (dataFlavor.isRepresentationClassRemote()) {
					System.out.println("isRepresentationClassRemote");
				} 

				if (dataFlavor.isRepresentationClassSerializable()) {
					System.out.println("isRepresentationClassSerializable");
					// ignore
//					ByteBuffer byteBuffer = (ByteBuffer) clipBoardContent;
//					byte[] bs = byteBuffer.array();
//					System.out.println(new String(bs));
				}
				
				System.out.println(dataFlavor.getMimeType());
				
			} catch (UnsupportedFlavorException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		
		
	}
	
	public static void main(String[] args) {
		TextPanelTest panelTest = new TextPanelTest();
		panelTest.setVisible(true);
		
//		Pattern pattern = Pattern.compile("\\<\\![\\-]{2}.+?[\\-]{2}\\>");
//		Matcher matcher = pattern.matcher("<!-- asfasdf -->-->");
//		String rs = matcher.replaceAll("");
//		System.out.println(rs);
//		if(matcher.matches()){
//			System.out.println(matcher.groupCount());
//			System.out.println(matcher.group());
//		}
	}
	

}
