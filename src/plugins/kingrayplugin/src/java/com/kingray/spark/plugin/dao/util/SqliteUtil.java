package com.kingray.spark.plugin.dao.util;

import org.jivesoftware.spark.util.log.Log;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.kingray.spark.plugin.vo.MessageVO;

public class SqliteUtil {
	private static String dbPath = "history/";
	private static String dbFileString = "messageHistory.db";
	private static String realDbFilePath = dbPath + dbFileString;
//	static {
//		File dbStorePath = new File(dbPath);
//		if (!dbStorePath.exists()) {
//			dbStorePath.mkdirs();
//		}
//		File dbStoreFile = new File(dbPath + dbFileString);
//		if (!dbStoreFile.exists()) {
//			try {
//				dbStoreFile.createNewFile();
//			} catch (IOException e) {
//				Log.error(e);
//			}
//		}
//		try {
//			realDbFilePath = dbStoreFile.getCanonicalPath();
//		} catch (IOException e) {
//			Log.error(e);
//		}
//	}

	/**
	 *
	 * 初始化数据库
	 * @param path 数据文件地址
	 * @return
	 */
	private static String initDBFile(String path) {
		if(path == null || "".equals(path.trim())){
			path = dbPath;
		}
		File dbStorePath = new File(path);// 数据存放的目录
		File dbStoreFile = dbStorePath; // 数据文件路径
		if (dbStorePath.isFile()) { // 如果数据存放的目录是文件地址，那么就重新指定数据存放的目录为父目录
			dbStorePath = dbStorePath.getParentFile();
		} else { // 如果指定路径是文件夹，那么使用默认的数据文件名
			dbStoreFile = new File(path, dbFileString);
		}
		if (!dbStorePath.exists()) { // 目录不存在，则创建目录
			dbStorePath.mkdirs();
		}

		if (!dbStoreFile.exists()) { // 文件不存在，则创建文件
			try {
				dbStoreFile.createNewFile();
			} catch (IOException e) {
				Log.error(e);
			}
		}
		String realFilePath = path; // 返回真实的文件名
		try {
			realFilePath = dbStoreFile.getCanonicalPath();
		} catch (IOException e) {

			Log.error(e);
		}
//		System.out.println("dbStorePath ========= " + dbStorePath);
//		System.out.println("dbStoreFile ========= " + dbStoreFile);
//		System.out.println("SqliteUtil.initDBFile realFilePath: " + realFilePath);
		return realFilePath;
	}

	/**
	 * 获取连接
	 * 
	 * @return
	 */
	public static Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:"
					+ realDbFilePath);
		} catch (ClassNotFoundException e) {
			Log.error(e);
		} catch (SQLException e) {
			Log.error(e);
		}
		return connection;
	}
	/**
	 * 获取指定数据库文件的连接对象
	 * 
	 * @param dbFilePath
	 *           数据库文件路径，如果为目录则使用默认的文件名
	 * @return
	 */
	public static Connection getConnection(String dbFilePath) {
		return getConnection(dbFilePath, null, null);
	}
	/**
	 * 获取指定数据库文件的连接对象，并指定用户名和密码（未实现）
	 * 
	 * @param dbFilePath
	 *            数据库文件路径，如果为目录则使用默认的文件名
	 * @param userName 用户名
	 * @param password 密码
	 * @return
	 */
	public static Connection getConnection(String dbFilePath, String userName, String password) {

		Connection connection = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:"
					+ initDBFile(dbFilePath), userName, password);
			connection.setAutoCommit(false);
			connection.prepareStatement("");
		} catch (ClassNotFoundException e) {
			Log.error(e);
		} catch (SQLException e) {
			Log.error(e);
		}
		return connection;
	}

	public static void closeConnectionStateMent(Connection connection,
			Statement statement) {
		closeConnection(connection);
		closeStateMent(statement);
	}

	public static void closeConnection(Connection connection) {
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			Log.error(e);
		}
	}

	public static void closeStateMent(Statement statement) {
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			Log.error(e);
		}
	}

	public static boolean isExistTable(Connection connection, String tableName) {
		boolean result = false;
		if (connection == null || tableName == null || "".equals(tableName)) { // ???????????????????
			return false;
		}
		try {
			ResultSet rs = connection.getMetaData().getTables(null, null,
					tableName, null);
			if (rs.next()) {
				result = true;
			}
		} catch (Exception e) {
			Log.error(e);
		}
		return result;
	}

//	public static void main(String[] args) {
//
//		List<MessageVO> messageVOs = null;
//		Connection connection = SqliteUtil.getConnection("");
//		try {
//			PreparedStatement preparedStatement = connection
//					.prepareStatement("select message_id, message_send_id, message_from, message_to, message_body, message_date_time from message_history where message_date_time >= '2013-05-30 21:05:52'  and message_date_time <=  '2013-05-30 21:05:53'  and message_from = '??????' or message_to = '??????'");
//			ResultSet rs = preparedStatement.executeQuery();
//			messageVOs = new ArrayList<MessageVO>();
//			while (rs.next()) {
//				int messageId = rs.getInt(1);
//				String sendMessageId = rs.getString(2);
//				String messageFrom = rs.getString(3);
//				String messageTo = rs.getString(4);
//				String messageBody = rs.getString(5);
//				String messageDateTimeStr = rs.getString(6);
//				MessageVO messageVO = new MessageVO(messageId, sendMessageId, messageFrom, messageTo, messageBody);
//				messageVO.setMessageDateTimeStr(messageDateTimeStr);
//				messageVOs.add(messageVO);
//			}
//		} catch (SQLException e) {
//			Log.error(e);
//		}
//		System.out.println(messageVOs);
//	}
}
