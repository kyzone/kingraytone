package com.kingray.spark.plugin.dao.impl;

import org.jivesoftware.spark.util.log.Log;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

import com.kingray.spark.plugin.dao.MessageHistoryDAO;
import com.kingray.spark.plugin.dao.util.SqliteUtil;
import com.kingray.spark.plugin.service.MessageService;
import com.kingray.spark.plugin.service.UserService;
import com.kingray.spark.plugin.vo.MessageVO;
import com.kingray.spark.plugin.vo.UserVO;

public class MessageHistoryDAOImpl implements MessageHistoryDAO {
	private static final int MAX_CONVERSATION_SIZE = 50;

	/**
	 * 创建历史消息表单
	 */
	private static final String SQL_CREATE_MESSAGE_HISTORY_TABLE = "CREATE table message_history("
			+ "message_id integer primary key autoincrement, "
			+ "message_send_id text UNIQUE, "
			+ "message_from text, "
			+ "message_to text, "
			+ "message_body text, "
			+ "message_date_time text, "
			+ "message_type text, "
			+ "FOREIGN KEY(message_from) REFERENCES user(user_name), "
			+ "FOREIGN KEY(message_to) REFERENCES user(user_name)" + ")";

	/**
	 * 创建历史消息索引
	 */
	private static final String SQL_CREATE_MESSAGE_HISTORY_INDEX = "CREATE INDEX message_history_index ON message_history(message_id DESC, message_send_id, message_date_time DESC)";

	/**
	 * 创建历史消息索引
	 */
	private static final String SQL_CREATE_MESSAGE_HISTORY_INDEX2 = "CREATE INDEX message_history_index2 ON message_history(message_from, message_to)";

	/**
	 * 创建用户表
	 */
	private static final String SQL_CREATE_USER_TABLE = "CREATE table user("
			+ "user_name text primary key, real_name text )";
	/**
	 * 插入外键
	 */
	private static final String SQL_ALTER_FOREIGN_KEY = "alter table message_history add constraint FK_Reference_2 foreign key (message_from) references user (user_name) on delete restrict on update restrict;";
	private static final String SQL_ALTER_FOREIGN_KEY2 = "alter table message_history add constraint FK_Reference_3 foreign key (message_to) references user (user_name) on delete restrict on update restrict;";

	/**
	 * 插入历史消息
	 */
	private static final String SQL_INSERT_MESSAGE_HISTORY = "INSERT INTO message_history(message_send_id, message_from, message_to, message_body, message_date_time, message_type) values(?, ?, ?, ?, ?, ?)";
	/**
	 * 查询最近会话
	 */
	// private static final String SQL_QUERY_USER_CONVERSATION =
	// "select message_id, message_send_id, message_from, message_to, message_body, message_date_time, message_type from message_history group by message_from, message_to, message_date_time order by message_date_time desc ";
	// private static final String SQL_QUERY_USER_CONVERSATION =
	// "select mh.message_id, mh.message_send_id, mh.message_from, mh.message_to, mh.message_body, mh.message_date_time, mh.message_type "
	// +
	// "from (select *,max(message_date_time) max_message_date_time from message_history group by message_from,message_to) m, message_history mh "
	// +
	// "where m.max_message_date_time = mh.message_date_time group by mh.message_from, mh.message_to order by mh.message_date_time desc ";
	private static final String SQL_QUERY_USER_CONVERSATION = "select mh.message_id, mh.message_send_id, mh.message_from, mh.message_to, mh.message_body, mh.message_date_time, mh.message_type from "
			+ "(select *,max(message_date_time) max_message_date_time from message_history group by message_from,message_to) m, message_history mh "
			+ "where m.max_message_date_time = mh.message_date_time "
			+ "and m.message_from = mh.message_from "
			+ "and m.message_to = mh.message_to "
			+ "group by mh.message_to, mh.message_from "
			+ "order by mh.message_date_time desc";

	/**
	 * 查询会话预览
	 */
	private static final String SQL_QUERY_USER_CONVERSATION_SUMMARY = "select message_id, message_send_id from message_history";

	/**
	 * 查询总消息数
	 */
	private static final String SQL_QUERY_CONVERSATION_COUNT = "select count(message_id) from message_history";

	/**
	 * 插入用户
	 */
	private static final String SQL_INSERT_USER = "INSERT INTO user(user_name, real_name) values(?, ?)";

	/**
	 * 更新用户信息
	 */
	private static final String SQL_UPDATE_USER = "UPDATE user set real_name = ? where user_name = ?";

	/**
	 * 查询所有用户
	 */
	private static final String SQL_QUERY_ALL_USER = "select user_name, real_name from user";
	/**
	 * 查询所有用户
	 */
	private static final String SQL_QUERY_USER_REAL_NAME_BY_USER_NAME = "select real_name from user where user_name = ?";

	/**
	 * 根据用户名查询历史消息
	 */
	private static final String SQL_QUERY_MESSAGE_HISTORIES_BY_USER_NAME = "select message_id, message_send_id, message_from, message_to, message_body, message_date_time, message_type from message_history "
			+ "where date(message_date_time) between date(?) and date(?) and message_from = ? or message_to = ? ";
	/**
	 * 根据用户名查询历史消息
	 */
	private static final String SQL_QUERY_MESSAGE_HISTORIES_BY_USER_NAME_WITHOUT_DATE = "select message_id, message_send_id, message_from, message_to, message_body, message_date_time, message_type from message_history "
			+ "where message_from = ? or message_to = ? order by message_date_time ";

	/**
	 * 根据用户名和消息内容查询历史消息
	 */
	private static final String SQL_QUERY_MESSAGE_HISTORIES_BY_USER_NAME_AND_MESSAGE_BODY_WITHOUT_DATE = "select message_id, message_send_id, message_from, message_to, message_body, message_date_time, message_type from message_history "
			+ "where (message_from = ? or message_to = ?) and  message_body like ?";

	private static final Object LOCK = new Object();

	private String historyPath;

	private static MessageHistoryDAOImpl singleton;

	public static MessageHistoryDAOImpl getInstance(String historyPath) {
		synchronized (LOCK) {
			if (singleton == null) {
				singleton = new MessageHistoryDAOImpl(historyPath);
			}
		}
		return singleton;
	}

	/**
	 * 创建连接对象
	 * 
	 * @param historyPath
	 *            数据文件的路径
	 */
	private MessageHistoryDAOImpl(String historyPath) {
		this.historyPath = historyPath;
		this.init();
		// if(historyPath == null){
		// historyPath = "";
		// }
	}

	/**
	 * 初始化数据库
	 * 
	 * @param historyPath
	 *            数据文件的路径
	 */
	public void init() {
		synchronized (LOCK) {
			Connection connection = getConnection();
			try {
				connection.setAutoCommit(false);
				Statement statement = connection.createStatement();
				if (!SqliteUtil.isExistTable(connection, "user")) {
					statement.executeUpdate(SQL_CREATE_USER_TABLE);
					// //添加外键放在此处是否合适？
					// statement.executeUpdate(SQL_ALTER_FOREIGN_KEY);
					// statement.executeUpdate(SQL_ALTER_FOREIGN_KEY2);
				}
				if (!SqliteUtil.isExistTable(connection, "message_history")) {
					statement.executeUpdate(SQL_CREATE_MESSAGE_HISTORY_TABLE);
					statement.executeUpdate(SQL_CREATE_MESSAGE_HISTORY_INDEX);
					statement.executeUpdate(SQL_CREATE_MESSAGE_HISTORY_INDEX2);
				}
				connection.commit();
				SqliteUtil.closeConnectionStateMent(connection, statement);
			} catch (SQLException e) {
				Log.error(e);
			} finally {
				SqliteUtil.closeConnection(connection);
			}
		}
	}

	/**
	 * 获取连接
	 * 
	 * @return
	 */
	public Connection getConnection() {
		return SqliteUtil.getConnection(historyPath);
	}

	public void insertMessageHistories(Collection messageHistories) {
		Connection connection = getConnection();
		try {
			connection.setAutoCommit(false);// 取消自动提交事务
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_INSERT_MESSAGE_HISTORY);
			for (Iterator iterator = messageHistories.iterator(); iterator
					.hasNext();) {
				MessageVO messageVO = (MessageVO) iterator.next();
				preparedStatement.setString(1, messageVO.getSendMessageId());
				preparedStatement.setString(2, messageVO.getMessageFrom()
						.getUserName());
				preparedStatement.setString(3, messageVO.getMessageTo()
						.getUserName());
				preparedStatement.setString(4, messageVO.getMessageBody());
				preparedStatement.setString(5,
						messageVO.getMessageDateTimeStr());
				preparedStatement.setString(6, messageVO.getMessageType());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
	}

	public void insertMessageHistory(MessageVO messageVO) {
		Connection connection = getConnection();
		try {
			connection.setAutoCommit(false);// 取消自动提交事务
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_INSERT_MESSAGE_HISTORY);
			preparedStatement.setString(1, messageVO.getSendMessageId());
			preparedStatement.setString(2, messageVO.getMessageFrom()
					.getUserName());
			preparedStatement.setString(3, messageVO.getMessageTo()
					.getUserName());
			preparedStatement.setString(4, messageVO.getMessageBody());
			preparedStatement.setString(5, messageVO.getMessageDateTimeStr());
			preparedStatement.setString(6, messageVO.getMessageType());
			preparedStatement.addBatch();
			preparedStatement.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
	}

	/*
	 * select message_id, message_send_id, message_from, message_to,
	 * message_date_time from message_history where message_date_time >=
	 * '2013-05-30 21:05:51' and message_date_time <= '2013-05-30 21:05:53' and
	 * message_from = '熊瑛琪' or message_to = '熊瑛琪'
	 */
	@Override
	public List<MessageVO> queryMessageHistoriesByUserName(String userName,
			String dateFrom, String dateTo) {
		List<MessageVO> messageVOs = null;
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_QUERY_MESSAGE_HISTORIES_BY_USER_NAME);
			preparedStatement.setString(1, dateFrom);
			preparedStatement.setString(2, dateTo);
			preparedStatement.setString(3, userName);
			preparedStatement.setString(4, userName);
			ResultSet rs = preparedStatement.executeQuery();
			messageVOs = new ArrayList<MessageVO>();
			while (rs.next()) {
				long messageId = rs.getLong(1);
				String sendMessageId = rs.getString(2);
				String messageFrom = rs.getString(3);
				String messageTo = rs.getString(4);
				String messageBody = rs.getString(5);
				String messageDateTimeStr = rs.getString(6);
				String messageType = rs.getString(7);
				MessageVO messageVO = new MessageVO();
				messageVO.setMessageId(messageId);
				messageVO.setSendMessageId(sendMessageId);
				messageVO.setMessageFrom(new UserVO(messageFrom));
				messageVO.setMessageTo(new UserVO(messageTo));
				messageVO.setMessageBody(messageBody);
				messageVO.setMessageType(messageType);
				messageVO.setMessageDateTimeStr(messageDateTimeStr);
				messageVOs.add(messageVO);
			}
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
		return messageVOs;
	}

	@Override
	public List<MessageVO> queryMessageHistoriesByUserName(String userName) {
		List<MessageVO> messageVOs = null;
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_QUERY_MESSAGE_HISTORIES_BY_USER_NAME_WITHOUT_DATE);
			preparedStatement.setString(1, userName);
			preparedStatement.setString(2, userName);
			ResultSet rs = preparedStatement.executeQuery();
			messageVOs = new ArrayList<MessageVO>();
			while (rs.next()) {
				long messageId = rs.getLong(1);
				String sendMessageId = rs.getString(2);
				String messageFrom = rs.getString(3);
				String messageTo = rs.getString(4);
				String messageBody = rs.getString(5);
				String messageDateTimeStr = rs.getString(6);
				String messageType = rs.getString(7);
				MessageVO messageVO = new MessageVO();
				messageVO.setMessageId(messageId);
				messageVO.setSendMessageId(sendMessageId);
				messageVO.setMessageFrom(new UserVO(messageFrom));
				messageVO.setMessageTo(new UserVO(messageTo));
				messageVO.setMessageBody(messageBody);
				messageVO.setMessageType(messageType);
				messageVO.setMessageDateTimeStr(messageDateTimeStr);
				messageVOs.add(messageVO);
			}
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
		return messageVOs;
	}

	@Override
	public Collection<UserVO> getAllUser() {
		Collection<UserVO> userVOs = null;
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_QUERY_ALL_USER);
			ResultSet rs = preparedStatement.executeQuery();
			userVOs = new HashSet<UserVO>();
			while (rs.next()) {
				String userName = rs.getString(1);
				String realName = rs.getString(2);
				UserVO userVO = new UserVO(userName, realName);
				userVOs.add(userVO);
			}
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
		return userVOs;
	}

	@Override
	public Collection<MessageVO> getAllConversations() {
		Collection<MessageVO> conversations = null;
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_QUERY_USER_CONVERSATION
							+ " limit 0, " + MAX_CONVERSATION_SIZE);
			ResultSet rs = preparedStatement.executeQuery();
			conversations = new LinkedHashSet<MessageVO>();
			while (rs.next()) {
				// select message_id, message_send_id, message_from, message_to,
				// message_body, message_date_time, message_type from
				// message_history group by message_from,message_to order by
				// message_date_time desc
				Long messageId = rs.getLong(1);
				String sendMessageId = rs.getString(2);
				String messageFrom = rs.getString(3);
				String messageTo = rs.getString(4);
				String messageBody = rs.getString(5);
				String messageDateTimeStr = rs.getString(6);
				String messageType = rs.getString(7);

				MessageVO messageVO = new MessageVO();
				messageVO.setMessageId(messageId);
				messageVO.setSendMessageId(sendMessageId);
				messageVO.setMessageFrom(new UserVO(messageFrom));
				messageVO.setMessageTo(new UserVO(messageTo));
				messageVO.setMessageBody(messageBody);
				messageVO.setMessageDateTimeStr(messageDateTimeStr);
				messageVO.setMessageType(messageType);

				conversations.add(messageVO);
			}
			// System.out.println("conversations.size() =============== " +
			// conversations.size());
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
		return conversations;
	}

	@Override
	public String findUserRealName(String userName) {
		String realName = null;
		Connection connection = getConnection();
		try {
			connection.setAutoCommit(true);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_QUERY_USER_REAL_NAME_BY_USER_NAME);
			preparedStatement.setString(1, userName);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				realName = rs.getString(1);
			}
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
		return realName;
	}

	@Override
	public void addUserVOs(Collection<UserVO> userVOs) { // SQL_INSERT_USER
		Connection connection = getConnection();
		try {
			connection.setAutoCommit(false);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		Collection<UserVO> existsUserVOs = getAllUser();// 数据库内已经存在的用户数据
		Collection<UserVO> toUpdateUserVOs = new HashSet<UserVO>(); // 需要更新的数据
		toUpdateUserVOs.addAll(userVOs); // 先加入要发生更改的数据
		userVOs.removeAll(existsUserVOs); // 剩下要添加的数据
		toUpdateUserVOs.removeAll(userVOs);// 剩下要更新的数据

		try {
			// 添加用户数据
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_INSERT_USER);
			for (Iterator iterator = userVOs.iterator(); iterator.hasNext();) {
				UserVO userVO = (UserVO) iterator.next();
				preparedStatement.setString(1, userVO.getUserName());
				preparedStatement.setString(2, userVO.getRealName());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();

			// 更新用户数据
			preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);
			for (Iterator iterator = toUpdateUserVOs.iterator(); iterator
					.hasNext();) {
				UserVO userVO = (UserVO) iterator.next();
				preparedStatement.setString(1, userVO.getRealName());
				preparedStatement.setString(2, userVO.getUserName());
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}

	}

	@Override
	public int getCoversationCount() {
		int count = -1;
		Connection connection = getConnection();
		try {
			connection.setAutoCommit(true);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_QUERY_CONVERSATION_COUNT);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
		return count;
	}

	@Override
	public Collection<MessageVO> getAllConversationSummary() {
		Collection<MessageVO> conversations = null;
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_QUERY_USER_CONVERSATION_SUMMARY);
			ResultSet rs = preparedStatement.executeQuery();
			conversations = new LinkedHashSet<MessageVO>();
			while (rs.next()) {
				// select message_id, message_send_id, message_from, message_to,
				// message_body, message_date_time, message_type from
				// message_history group by message_from,message_to order by
				// message_date_time desc
				Long messageId = rs.getLong(1);
				String sendMessageId = rs.getString(2);

				MessageVO messageVO = new MessageVO();
				messageVO.setMessageId(messageId);
				messageVO.setSendMessageId(sendMessageId);

				conversations.add(messageVO);
			}
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
		return conversations;
	}

	/**
	 * <br>
	 * 2013-6-25 下午4:54:14
	 * 
	 * @see com.kingray.spark.plugin.dao.MessageHistoryDAO#searcheMessageHistoriesByMessageBody(java.lang.String,
	 *      java.lang.String)
	 * 
	 */
	@Override
	public Collection<MessageVO> searcheMessageHistoriesByMessageBody(
			String relationUserName, String queryMessageBody) {
		List<MessageVO> messageVOs = null;
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement(SQL_QUERY_MESSAGE_HISTORIES_BY_USER_NAME_AND_MESSAGE_BODY_WITHOUT_DATE);
			preparedStatement.setString(1, relationUserName);
			preparedStatement.setString(2, relationUserName);
			if (queryMessageBody == null) {
				queryMessageBody = "";
			}
			preparedStatement.setString(3, "%" + queryMessageBody + "%");
			ResultSet rs = preparedStatement.executeQuery();
			messageVOs = new ArrayList<MessageVO>();
			while (rs.next()) {
				long messageId = rs.getLong(1);
				String sendMessageId = rs.getString(2);
				String messageFrom = rs.getString(3);
				String messageTo = rs.getString(4);
				String messageBody = rs.getString(5);
				String messageDateTimeStr = rs.getString(6);
				String messageType = rs.getString(7);
				MessageVO messageVO = new MessageVO();
				messageVO.setMessageId(messageId);
				messageVO.setSendMessageId(sendMessageId);
				messageVO.setMessageFrom(new UserVO(messageFrom));
				messageVO.setMessageTo(new UserVO(messageTo));
				messageVO.setMessageBody(messageBody);
				messageVO.setMessageType(messageType);
				messageVO.setMessageDateTimeStr(messageDateTimeStr);
				messageVOs.add(messageVO);
			}
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
		return messageVOs;
	}

	/**
	 * <br>
	 * 2013-6-26 上午9:53:41
	 * 
	 * @see com.kingray.spark.plugin.dao.MessageHistoryDAO#queryMessageHistoriesByUserName(java.lang.String,
	 *      int)
	 */
	@Override
	public Collection<MessageVO> queryMessageHistoriesByUserName(
			String userName, int recordCount, boolean desc) {
		List<MessageVO> messageVOs = null;
		Connection connection = getConnection();
		try {
			String query_sql = SQL_QUERY_MESSAGE_HISTORIES_BY_USER_NAME_WITHOUT_DATE;
			if (desc) {
				query_sql += " desc ";
			}

			PreparedStatement preparedStatement = connection
					.prepareStatement(query_sql + " limit 0, ?");
			preparedStatement.setString(1, userName);
			preparedStatement.setString(2, userName);
			preparedStatement.setInt(3, recordCount);
			ResultSet rs = preparedStatement.executeQuery();
			messageVOs = new ArrayList<MessageVO>();
			while (rs.next()) {
				long messageId = rs.getLong(1);
				String sendMessageId = rs.getString(2);
				String messageFrom = rs.getString(3);
				String messageTo = rs.getString(4);
				String messageBody = rs.getString(5);
				String messageDateTimeStr = rs.getString(6);
				String messageType = rs.getString(7);
				MessageVO messageVO = new MessageVO();
				messageVO.setMessageId(messageId);
				messageVO.setSendMessageId(sendMessageId);
				messageVO.setMessageFrom(new UserVO(messageFrom));
				messageVO.setMessageTo(new UserVO(messageTo));
				messageVO.setMessageBody(messageBody);
				messageVO.setMessageType(messageType);
				messageVO.setMessageDateTimeStr(messageDateTimeStr);
				messageVOs.add(messageVO);
			}
		} catch (SQLException e) {
			Log.error(e);
		} finally {
			SqliteUtil.closeConnection(connection);
		}
		return messageVOs;
	}

	public static void main(String[] args) {
		MessageHistoryDAO dao = new MessageHistoryDAOImpl("");
		// List messageHistories = new ArrayList<MessageVO>();
		// for (int i = 0; i < 10; i++) {
		// MessageVO messageVO = new MessageVO();
		// messageVO.setMessageDateTime(new Date());
		// messageVO
		// .setMessageType(org.jivesoftware.smack.packet.Message.Type.chat
		// + "");
		// messageHistories.add(messageVO);
		// dao.insertMessageHistory(messageVO);
		// }
		// dao.insertMessageHistories(messageHistories);

		Collection<MessageVO> rs = dao.queryMessageHistoriesByUserName("胡金云",
				2, true);
		System.out.println(rs);
	}

}
