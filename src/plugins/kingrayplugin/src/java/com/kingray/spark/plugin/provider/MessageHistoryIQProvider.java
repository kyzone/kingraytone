package com.kingray.spark.plugin.provider;

import java.io.IOException;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.kingray.spark.packet.KingrayNameSpace;
import com.kingray.spark.plugin.packet.MessageHistoryResultIQ;
import com.kingray.spark.plugin.packet.MessageHistoryIQ.MessageHistoryActionType;
import com.kingray.spark.plugin.vo.MessageVO;
import com.kingray.spark.plugin.vo.UserVO;
import com.xiongyingqi.util.StringHelper;

import org.jivesoftware.spark.util.log.Log;
/**
 * IQ消息处理类，用于处理服务器返回的数据
 * 
 * @author KRXiongYingqi
 * 
 */
public class MessageHistoryIQProvider implements IQProvider {
	private static final KingrayNameSpace QUERY_MESSAGE = KingrayNameSpace.QUERY_MESSAGE;

	@Override
	public IQ parseIQ(XmlPullParser parser) throws Exception {

		// SipAccountPacket packet = new SipAccountPacket();
		// SipAccount sip = new SipAccount();

		MessageHistoryResultIQ packet = new MessageHistoryResultIQ();

		String actionType = parser.getAttributeValue("", "actionType");

		if (StringHelper.notNullAndNotEmpty(actionType)) {
			switch (MessageHistoryActionType.valueOf(actionType)) {
			case QUERY_COUNT:
				packet = parseQueryCount(parser);
				break;
			case QUERY_SUMMARY:
				packet = parseQuerySummary(parser);
				break;
			case QUERY_DETAIL:
				packet = parseQueryDetail(parser);
				break;
			default:
				break;
			}
		}

		// packet.setSipAcccount(sip);
		// return packet;
		return packet;
	}

	public MessageHistoryResultIQ parseQueryCount(XmlPullParser parser) throws Exception {
		MessageHistoryResultIQ iq = new MessageHistoryResultIQ();
		String messageHistoryCountStr = parser.getAttributeValue("", "messageHistoryCount");
		String relationUserName = parser.getAttributeValue("", "relationUserName");
		if(StringHelper.notNullAndNotEmpty(messageHistoryCountStr)){
			try {
				int messageHistoryCount = Integer.parseInt(messageHistoryCountStr);
				iq.setMessageCount(messageHistoryCount);
			} catch (Exception e) {
				Log.error(e);
			}
		}
		if(StringHelper.notNullAndNotEmpty(relationUserName)){
			try {
				iq.setRelationUserName(relationUserName);
			} catch (Exception e) {
				Log.error(e);
			}
		}
		return iq;
	}

	public MessageHistoryResultIQ parseQuerySummary(XmlPullParser parser) throws Exception {
		MessageHistoryResultIQ iq = new MessageHistoryResultIQ();
		boolean done = false;
		while (!done) {
			int eventType = parser.next();
			if (eventType == XmlPullParser.START_TAG) {
				if (parser.getName().equals("message")) {
					MessageVO messageVO = new MessageVO();
					String messageIdStr = parser.getAttributeValue("", "messageId");
					String sendMessageId = parser.getAttributeValue("", "sendMessageId");
					if(StringHelper.notNullAndNotEmpty(messageIdStr)){
						int messageId = Integer.parseInt(messageIdStr);
						messageVO.setMessageId(messageId);
					}
					if(StringHelper.notNullAndNotEmpty(sendMessageId)){
						messageVO.setSendMessageId(sendMessageId);
					}
					iq.addMessageVO(messageVO);
				}
			} else if (eventType == XmlPullParser.END_TAG) {
				if (parser.getName().equals(QUERY_MESSAGE.getKey())) {
					done = true;
				}
			}
		}
		return iq;
	}

	public MessageHistoryResultIQ parseQueryDetail(XmlPullParser parser) throws Exception {
		MessageHistoryResultIQ iq = new MessageHistoryResultIQ();
		boolean done = false;
		while (!done) {
			int eventType = parser.next();
			if (eventType == XmlPullParser.START_TAG) {
				if (parser.getName().equals("message")) {
					MessageVO messageVO = new MessageVO();
					String messageIdStr = parser.getAttributeValue("", "messageId");
					String sendMessageId = parser.getAttributeValue("", "sendMessageId");
					String messageFrom = parser.getAttributeValue("", "messageFrom");
					String messageTo = parser.getAttributeValue("", "messageTo");
					String messageBody = parser.getAttributeValue("", "messageBody");
//					String messageDateTime = parser.getAttributeValue("", "messageDateTime"); messageVO已实现该逻辑，故舍弃
					String messageDateTimeStr = parser.getAttributeValue("", "messageDateTimeStr");
					String messageType = parser.getAttributeValue("", "messageType");
					if(StringHelper.notNullAndNotEmpty(messageIdStr)){
						int messageId = Integer.parseInt(messageIdStr);
						messageVO.setMessageId(messageId);
					}
					if(StringHelper.notNullAndNotEmpty(sendMessageId)){
						messageVO.setSendMessageId(sendMessageId);
					}
					if(StringHelper.notNullAndNotEmpty(messageFrom)){
						messageVO.setMessageFromStr(messageFrom);
					}
					if(StringHelper.notNullAndNotEmpty(messageTo)){
						messageVO.setMessageToStr(messageTo);
					}
					if(StringHelper.notNullAndNotEmpty(messageBody)){
						messageVO.setMessageBody(messageBody);
					}
//					if(StringHelper.notNullAndNotEmpty(messageDateTime)){ // messageVO已实现该逻辑，故舍弃
//						messageVO.setSendMessageId(sendMessageId);
//					}
					if(StringHelper.notNullAndNotEmpty(messageDateTimeStr)){
						messageVO.setMessageDateTimeStr(messageDateTimeStr);
					}
					if(StringHelper.notNullAndNotEmpty(messageType)){
						messageVO.setMessageType(messageType);
					}
					
					iq.addMessageVO(messageVO);
				}
			} else if (eventType == XmlPullParser.END_TAG) {
				if (parser.getName().equals(QUERY_MESSAGE.getKey())) {
					done = true;
				}
			}
		}
		return iq;
	}

}
