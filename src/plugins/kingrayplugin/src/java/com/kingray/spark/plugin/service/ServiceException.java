/**
 * spark_src
 */
package com.kingray.spark.plugin.service;

import com.xiongyingqi.exception.BaseException;

/**
 * 服务异常
 * @author XiongYingqi
 * @version 2013-6-24 下午4:51:57
 */
public class ServiceException extends BaseException {
	public ServiceException(){
		
	}
	public ServiceException(String message){
		super(message);
	}
	public ServiceException(String message, Throwable throwable){
		super(message, throwable);
	}
	public ServiceException(Throwable throwable){
		super(throwable);
	}
}
