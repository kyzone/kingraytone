package com.kingray.spark.plugin.packet;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.jivesoftware.smack.packet.IQ;

import com.kingray.spark.packet.KingrayNameSpace;
import com.xiongyingqi.utils.DateHelper;
import com.xiongyingqi.utils.StringHelper;

/**
 * 消息历史IQ
 * @author XiongYingqi
 *
 */
public class MessageHistoryIQ extends IQ {
	private static final KingrayNameSpace QUERY_MESSAGE = KingrayNameSpace.QUERY_MESSAGE;
	
	private Collection<Long> messageIds; // 服务器数据库的存储消息id
	private Collection<String> sendMessageIds; // 发送消息ID
	private String relationUserName;  // 关联用户
	private Date messageDateTime;  // 消息日期
	private String messageDateTimeStr; //  消息日期字符串
	private MessageHistoryActionType actionType;
	
	/**
	 * MessageHistoryActionType
	 * @return the actionType
	 */
	public MessageHistoryActionType getActionType() {
		return actionType;
	}

	/**
	 * @param actionType the actionType to set
	 */
	public void setActionType(MessageHistoryActionType actionType) {
		this.actionType = actionType;
	}

	/**
	 * @return the messageDateTimeStr
	 */
	public String getMessageDateTimeStr() {
		return messageDateTimeStr;
	}

	/**
	 * @param messageDateTimeStr the messageDateTimeStr to set
	 */
	public void setMessageDateTimeStr(String messageDateTimeStr) {
		this.messageDateTimeStr = messageDateTimeStr;
		this.messageDateTime = DateHelper.strToDate(messageDateTimeStr); // 字符串转换为日期格式
	}

	/**
	 * @return the relationUserName
	 */
	public String getRelationUserName() {
		return relationUserName;
	}

	/**
	 * @param relationUserName the relationUserName to set
	 */
	public void setRelationUserName(String relationUserName) {
		this.relationUserName = relationUserName;
	}

	/**
	 * @return the messageDateTime
	 */
	public Date getMessageDateTime() {
		return messageDateTime;
	}

	/**
	 * @param messageDateTime the messageDateTime to set
	 */
	public void setMessageDateTime(Date messageDateTime) {
		this.messageDateTime = messageDateTime;
		this.messageDateTimeStr = DateHelper.FORMATTER_SHORT.format(messageDateTime); // 转换为短日期格式
	}

	private String getNameSpace(){
		return QUERY_MESSAGE.getValue();
	}

	/**
	 * Collection<Integer>
	 * @return the messageIds
	 */
	public Collection<Long> getMessageIds() {
		return messageIds;
	}

	/**
	 * @param messageIds the messageIds to set
	 */
	public void setMessageIds(Collection<Long> messageIds) {
		this.messageIds = messageIds;
	}

	/**
	 * Collection<String>
	 * @return the sendMessageIds
	 */
	public Collection<String> getSendMessageIds() {
		return sendMessageIds;
	}

	/**
	 * @param sendMessageIds the sendMessageIds to set
	 */
	public void setSendMessageIds(Collection<String> sendMessageIds) {
		this.sendMessageIds = sendMessageIds;
	}

	@Override
	public String getChildElementXML() {
//		Document document = DocumentHelper.createDocument(); // 创建document对象
		Element root = DocumentHelper.createElement(QUERY_MESSAGE.getKey());// 添加自定义节点
//		Element root = document.addElement(QUERY_MESSAGE.getKey()); // 添加自定义节点
		root.addNamespace("", getNameSpace()); // 加入命名空间
		if(actionType != null){ // 设置业务类型
			root.addAttribute("actionType", actionType.toString());
		}
		if(relationUserName != null && !"".equals(relationUserName)){
			root.addAttribute("relationUserName", relationUserName); // 添加关联用户
		}
		if(messageDateTimeStr != null && !"".equals(messageDateTimeStr)){
			root.addAttribute("messageDateTimeStr", messageDateTimeStr); // 添加查询日期
		}
		if(messageIds != null && messageIds.size() > 0){
			for (Iterator<Long> iterator = messageIds.iterator(); iterator.hasNext();) {
				long messageId = iterator.next();
				Element messageElement = DocumentHelper.createElement("message");
				messageElement.addAttribute("messageId", messageId + ""); // 添加远程数据库ID
				root.add(messageElement); // 拼入根节点
			}
		}
		if(sendMessageIds != null && sendMessageIds.size() > 0){
			for (Iterator<String> iterator = sendMessageIds.iterator(); iterator.hasNext();) {
				String sendMessageId = iterator.next();
				if(StringHelper.notNullAndNotEmpty(sendMessageId)){
					Element messageElement = DocumentHelper.createElement("message");
					messageElement.addAttribute("sendMessageId", sendMessageId); // 添加远程数据库ID
					root.add(messageElement); // 拼入根节点
				}
			}
		}
		return  root.asXML();
	}
	
	public enum MessageHistoryActionType{
		/**
		 * 查询总条数
		 */
		QUERY_COUNT, 
		/**
		 * 查询概要消息
		 */
		QUERY_SUMMARY,
		/**
		 * 查询详细消息
		 */
		QUERY_DETAIL, 
	}
	
	public static void main(String[] args) {
		MessageHistoryIQ messageHistoryIQ = new MessageHistoryIQ();
		messageHistoryIQ.setActionType(MessageHistoryActionType.QUERY_SUMMARY);
		System.out.println(messageHistoryIQ.toXML());
//		try {
//			for (Method method : Class.forName("com.kingray.spark.plugin.packet.IQType").getMethods()) {
//				
//			}
//		} catch (SecurityException e) {
//			Log.error(e);
//		} catch (ClassNotFoundException e) {
//			Log.error(e);
//		}
//		 try {
//			Class clazz = Class.forName("com.kingray.spark.plugin.packet.MessageHistoryIQ");
//			boolean isAnnotation = clazz.isAnnotationPresent(IQType.class);
//			if(isAnnotation){
//				IQType iqtype = (IQType) clazz.getAnnotation(IQType.class);
//				System.out.println(iqtype.value().getValue());
//			}
//		} catch (ClassNotFoundException e) {
//			Log.error(e);
//		}
	}

}
