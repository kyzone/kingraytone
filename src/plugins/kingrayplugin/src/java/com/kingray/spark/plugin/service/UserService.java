package com.kingray.spark.plugin.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterGroup;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.HostedRoom;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.RoomInfo;
import org.jivesoftware.spark.SparkManager;
import org.jivesoftware.spark.util.log.Log;

import com.kingray.spark.plugin.KingrayPlugin;
import com.kingray.spark.plugin.dao.MessageHistoryDAO;
import com.kingray.spark.plugin.dao.impl.MessageHistoryDAOImpl;
import com.kingray.spark.plugin.vo.UserVO;
import com.xiongyingqi.util.DomainHelper;
import com.xiongyingqi.util.StringHelper;

/**
 * 用户服务，用户添加用户或查询用户
 * 
 * @author XiongYingqi
 * 
 */
public class UserService implements Service{
	/**
	 * 用户名的缓存, 用于快速查询
	 */
	private static Map<String, String> userVOCache;
	private static MessageHistoryDAO messageHistoryDAO;

	private static UserService singleton;
	private static final Object LOCK = new Object();

	public static UserService getInstance() {
		synchronized (LOCK) {
			if (singleton == null) {
				singleton = new UserService();
			}
		}
		return singleton;
	}

	private UserService() {
		messageHistoryDAO = MessageHistoryDAOImpl.getInstance(KingrayPlugin
				.getMessageHistoryPath());
		userVOCache = new HashMap<String, String>();
	}

	private void initUsers() {
		Collection<UserVO> userVOs = loadAllUser();
		userVOs.addAll(loadAllGroups());
		Collection<UserVO> persistentUserVOs = messageHistoryDAO.getAllUser(); // 数据库中的
		userVOs.removeAll(persistentUserVOs); // 减去交集
		messageHistoryDAO.addUserVOs(userVOs); // 添加新用户
		for (Iterator iterator = userVOs.iterator(); iterator.hasNext();) {
			UserVO userVO = (UserVO) iterator.next();
			userVOCache.put(userVO.getUserName(), userVO.getRealName());
		}
		for (Iterator iterator = persistentUserVOs.iterator(); iterator
				.hasNext();) {
			UserVO userVO = (UserVO) iterator.next();
			userVOCache.put(userVO.getUserName(), userVO.getRealName());
		}
	}

	public String getRealName(String userName) {
		String realName = userVOCache.get(userName);
		if (realName == null) {
			realName = messageHistoryDAO.findUserRealName(userName);
		}
		if(StringHelper.nullOrEmpty(realName)){
			realName = userName;
		}
		return realName;
	}

	/**
	 * 加载服务器的所有用户
	 * 
	 * @return
	 */
	private Collection<UserVO> loadAllUser() {
		Collection<UserVO> userVOs = new HashSet<UserVO>();
		Roster roster = SparkManager.getConnection().getRoster();
		Collection<RosterEntry> rosterEntries = roster.getEntries();
		for (Iterator iterator = rosterEntries.iterator(); iterator.hasNext();) {
			RosterEntry rosterEntry = (RosterEntry) iterator.next();
			String userName = DomainHelper.removeDomain(rosterEntry.getUser());
			String realName = rosterEntry.getName();
			UserVO userVO = new UserVO(userName, realName);
			userVOs.add(userVO);
		}
		return userVOs;
	}

	private Collection<UserVO> loadAllGroups() {
		Collection<UserVO> userVOs = new HashSet<UserVO>();
		try {
			Collection serviceNames = MultiUserChat.getServiceNames(SparkManager.getConnection());
			for (Iterator iterator = serviceNames.iterator(); iterator
					.hasNext();) {
				String serviceName = (String) iterator.next();
				Collection<HostedRoom> hostedRooms = MultiUserChat.getHostedRooms(
						SparkManager.getConnection(), serviceName);
//				System.out.println("serviceName =========== " + serviceName);
				for (Iterator iterator2 = hostedRooms.iterator(); iterator2.hasNext();) {
					HostedRoom hostedRoom = (HostedRoom) iterator2.next();
					String userName = DomainHelper
							.removeDomain(hostedRoom.getJid());
					String realName = hostedRoom.getName();
//					System.out.println("userName ======= " + userName);
//					System.out.println("realName ======= " + realName);
					UserVO userVO = new UserVO(userName, realName);
					userVOs.add(userVO);
				}
			}
		} catch (XMPPException e) {
			Log.error(e);
		}
		return userVOs;
	}
	// private Collection<UserVO> loadAllGroups() {
	// Collection<UserVO> userVOs = new HashSet<UserVO>();
	// try {
	// Collection<HostedRoom> result = MultiUserChat.getHostedRooms(
	// SparkManager.getConnection(), SparkManager.getConnection()
	// .getServiceName());
	// boolean stillSearchForOccupants = true;
	// try {
	// for (Object aResult : result) {
	// HostedRoom hostedRoom = (HostedRoom) aResult;
	// String roomName = hostedRoom.getName();
	// String roomJID = hostedRoom.getJid();
	// int numberOfOccupants = -1;
	// if (stillSearchForOccupants) {
	// RoomInfo roomInfo = null;
	// try {
	// roomInfo = MultiUserChat.getRoomInfo(
	// SparkManager.getConnection(), roomJID);
	// } catch (Exception e) {
	// // Nothing to do
	// }
	//
	// if (roomInfo != null) {
	// numberOfOccupants = roomInfo.getOccupantsCount();
	// if (numberOfOccupants == -1) {
	// stillSearchForOccupants = false;
	// }
	// } else {
	// stillSearchForOccupants = false;
	// }
	// }
	// UserVO userVO = new UserVO();
	// userVO.setUserName(DomainHelper.removeDomain(roomJID));
	// userVO.setRealName(roomName);
	// System.out.println("userVO.getUserName() ========== " +
	// userVO.getUserName());
	// System.out.println("userVO.getRealName() ========== " +
	// userVO.getRealName());
	// userVOs.add(userVO);
	// }
	// } catch (Exception e) {
	// Log.error("Error setting up GroupChatTable", e);
	// }
	// } catch (XMPPException e) {
	// Log.error(e);
	// }
	// return userVOs;
	// }

	/**
	 * <br>2013-6-24 下午4:42:47
	 * @see com.kingray.spark.plugin.service.Service#startService()
	 */
	@Override
	public void startService() {
		try {
			initUsers();
		} catch (Exception e) {
		}
	}

}
