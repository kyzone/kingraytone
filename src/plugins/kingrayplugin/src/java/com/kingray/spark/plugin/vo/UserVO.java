package com.kingray.spark.plugin.vo;

import com.kingray.spark.plugin.service.UserService;
import com.xiongyingqi.utils.DomainHelper;
import com.xiongyingqi.utils.EntityHelper;
import com.xiongyingqi.utils.StringHelper;

public class UserVO extends EntityHelper {
	private String userName;
	private String realName;
	public UserVO(){
	}
	/**
	 * @param userName 用户名
	 * @param realName 真实姓名
	 */
	public UserVO(String userName, String realName){
		setUserName(userName);
		setRealName(realName);
	}
	/**
	 * @param userName 用户名
	 * @param realName 真实姓名
	 */
	public UserVO(String userName){
		userName = DomainHelper.removeDomain(userName);
		setUserName(userName);
		String realName = UserService.getInstance().getRealName(userName);
		if(realName == null ||"".equals(realName) ){
			this.realName = userName;
		}
		
//		setRealName(UserService.getInstance().getRealName(userName)); // 获取真实姓名
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the realName
	 */
	public String getRealName() {
		if(StringHelper.nullOrEmpty(realName)){
			realName = UserService.getInstance().getRealName(userName);
			if(StringHelper.nullOrEmpty(realName)){
				return userName;
			}
		}
		return realName;
	}
	/**
	 * @param realName the realName to set
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}
	/**
	 * 重写hashCode，以用户名作为键值
	 * <br>2013-6-25 下午2:39:25
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		if(userName != null){
			return userName.hashCode();
		}
		return super.hashCode();
	}
	/**
	 * 重写equals，当使用hashSet存储数据时方便去重
	 * <br>2013-6-25 下午2:40:03
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(obj != null && (obj instanceof UserVO)){
			return obj.hashCode() == this.hashCode();
		}
		return false;
	}
	
	
}
