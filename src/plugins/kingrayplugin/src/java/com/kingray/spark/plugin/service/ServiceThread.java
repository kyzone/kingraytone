/**
 * 
 */
package com.kingray.spark.plugin.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jivesoftware.spark.util.log.Log;

import com.xiongyingqi.util.EntityHelper;
import com.xiongyingqi.util.StackTraceHelper;

/**
 * 使用线程启动服务，要使用本服务的Service类必须实现Service类
 * @author XiongYingqi
 * @version 2013-6-24 下午4:38:57
 */
public class ServiceThread implements Runnable{
//	private static Collection<Service> services;
	private static Map<Service, Integer> services;
	private static int curPriority; // 当前优先级
	
	static{
		services = new LinkedHashMap<Service, Integer>();
		curPriority = Integer.MAX_VALUE / 3 * 2; // 初始化优先级
	}
	/**
	 * 添加要调用的服务
	 * <br>2013-6-24 下午4:42:01
	 * @param service 要添加的服务
	 */
	public static void addService(Service service){
		addService(service, curPriority++);
	}
	/**
	 * 添加要调用的服务
	 * <br>2013-6-24 下午4:42:01
	 * @param service 要添加的服务
	 * @param priority 优先级，优先级越低越优先处理
	 */
	public static void addService(Service service, int priority){
		StackTraceHelper.printStackTrace();
		if (services.containsKey(priority)) { // 如果优先级已经存在，则增加优先级
			addService(service, ++priority);
			return; // 防止栈溢出
		}
		services.put(service, priority);
	}
	
	/**
	 * 启动服务，调用所有实现了Service接口的类
	 * <br>2013-6-24 下午4:48:07
	 */
	public static void startServices(){
		ServiceThread serviceThread = new ServiceThread();
		Thread thread = new Thread(serviceThread);
		thread.start();
	}
	
	/**
	 * 按照优先级对Map进行排序
	 * <br>2013-6-24 下午5:40:28
	 */
	private List<Entry<Service,Integer>> sortServicesByPriority(){
		ArrayList<Entry<Service,Integer>> list = new ArrayList<Entry<Service,Integer>>(services.entrySet()); 
		Collections.sort(list, new Comparator<Map.Entry<Service, Integer>>() {
			@Override
			public int compare(Entry<Service, Integer> o1,
					Entry<Service, Integer> o2) {
				Integer value1 = o1.getValue();
				Integer value2 = o2.getValue();
				return value1.compareTo(value2); 
			}
		});
		
		return list;
	}
	
	@Override
	public void run() {
		List<Entry<Service,Integer>> serviceList = sortServicesByPriority(); // 获取按优先级排序后的服务列表
		for (Iterator<Entry<Service, Integer>> iterator = serviceList.iterator(); iterator.hasNext();) {
			Entry<Service, Integer> entry = iterator.next();
			Service service = entry.getKey();
			if(service != null){
				try {
					System.out.println("run -------------- " + service.getClass());
					Log.debug("run -------------- " + service.getClass());
					service.startService();
				} catch (ServiceException e) {
					e.printStackTrace();
					Log.error(e);
				}
			}
		}
//		for (Iterator iterator = services.entrySet().iterator(); iterator.hasNext();) {
//			Entry<Service, Integer> entry = (Entry<Service, Integer>) iterator.next();
//			Service service = entry.getKey();
//			if(service != null){
//				try {
//					service.startService();
//				} catch (ServiceException e) {
//					Log.error(e);
//				}
//			}
//		}
	}
	
}
